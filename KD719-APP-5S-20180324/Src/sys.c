#include "sys.h"  

iapfun jump2app; 

struct
{
    u8 Key_IN_Flag:1;
    u8 Key_IN_Long:1;
    u8 Key_Value_Last:8;
	u8 Key_Value_Saved:8;
    u8 Key_IN_Filter:8;
	u8 Key_Long_Filter:8;
	u8 Key_SCAN_State:1;
	u8 Key_SCAN_OK:1;
	u8 Key_Value0:8;
}Key_Status;


//THUMB指令不支持汇编内联
//采用如下方法实现执行汇编指令WFI  
__asm void WFI_SET(void)
{
	WFI;		  
}
//关闭所有中断(但是不包括fault和NMI中断)
__asm void INTX_DISABLE(void)
{
	CPSID   I
	BX      LR	  
}
//开启所有中断
__asm void INTX_ENABLE(void)
{
	CPSIE   I
	BX      LR  
}
//设置栈顶地址
//addr:栈顶地址
__asm void MSR_MSP(u32 addr) 
{
	MSR MSP, r0 			//set Main Stack value
	BX r14
}


//跳转到应用程序段
//appxaddr:用户代码起始地址.
void iap_load_app(u32 appxaddr)
{
	if(((*(u32*)appxaddr)&0x2FFE0000)==0x20000000)	//检查栈顶地址是否合法.
	{ 
		jump2app=(iapfun)*(u32*)(appxaddr+4);		//用户代码区第二个字为程序开始地址(复位地址)		
		MSR_MSP(*(u32*)appxaddr);					//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
		jump2app();									//跳转到APP.
	}
}	

#if KEY_SCAN_EN
/***********************************************************************\
按键扫描程序															*
Key_Value0：															*
bit7：1代表有有效按键值，0无有效按键									*
bit6：1表示长按有效，0非长按											*
bit5-bit0：分别对应6个按键												*
|-----------------------------------------------------------||			*
|-----------------------|-按键分布-|------------------------||			*
|-----------------------------------------------------------||			*
|       名称        ||      短按        ||       长按 		||			*
|-----------------------------------------------------------||			*
|					||					||					||			*
|	UP	|			||	82	|			||	C2	|			||			*
|-------------------||------------------||------------------||			*
|	MOD	|	DD	|	||	88	|	90	|	||	C8	|	D0	|	||			*
|-------------------||------------------||------------------||			*
|	DOWN|	POW	|	||	84  |	81	|	||	C4  |	C1	|	||			*
|-------------------||------------------||------------------||			*
|		|WALK|		||		|A0|		||		|E0|		||			*
|					||					||					||			*
|-----------------------------------------------------------||			*
************************************************************************/
u8 Key_Scan_Handler(void)
{
	if(Key_Status.Key_SCAN_State)
	{
			__nop();__nop();
			if((!Key_MOD) || (!Key_L2) || (!Key_L3))
			{
				if(!Key_MOD) 
					Key_Status.Key_Value0 &= 0xf7;
				else
					Key_Status.Key_Value0 &= 0xfe;
				if(!Key_L2) 
					Key_Status.Key_Value0 &= 0xef;
				else
					Key_Status.Key_Value0 &= 0xfd;
				if(!Key_L3) 
					Key_Status.Key_Value0 &= 0xdf;
				else
					Key_Status.Key_Value0 &= 0xfb;
			}
			else
			{
				Key_Status.Key_Value0 &=0xf8;
			}
			
			Key_Status.Key_SCAN_State=0;
			Key_R_0;
			Key_Status.Key_SCAN_OK=1;
	}
	else
	{
			Key_Status.Key_SCAN_OK=0;
			Key_Status.Key_Value0=0;
			if((!Key_MOD) || (!Key_L2) || (!Key_L3))//有按键按下
			{
					if(!Key_MOD)
						Key_Status.Key_Value0 |= 0x09;
					if(!Key_L2) 
						Key_Status.Key_Value0 |= 0x12;
					if(!Key_L3) 
						Key_Status.Key_Value0 |= 0x24;
					Key_R_1;//准备检查下一行按键
					Key_Status.Key_SCAN_State=1;
			}
			else//没有按键
			{
					Key_Status.Key_Value0=0;
					Key_Status.Key_SCAN_State=0;
					Key_R_0;
			}
	}
	if(Key_Status.Key_Value0)
	{
		if(Key_Status.Key_SCAN_OK)
		{
			if(Key_Status.Key_Value0==Key_Status.Key_Value_Last)
			{
					if(!Key_Status.Key_IN_Flag)
					{
							if(++Key_Status.Key_IN_Filter>1)
							{
									Key_Status.Key_IN_Flag=1;
									Key_Status.Key_Value_Saved=Key_Status.Key_Value_Last;
							}
					}
					else
					{
							if(!Key_Status.Key_IN_Long)//确保长按时，只返回一次有效键值，直至按键松开
							{
									if(++Key_Status.Key_Long_Filter>40)//长按有效
									{
											Key_Status.Key_IN_Long=1;
											Key_Status.Key_Long_Filter=0;
											Key_Status.Key_Value0 = Key_Status.Key_Value_Last | 0XC0;
											return Key_Status.Key_Value0;
									}
							}
							else
									return Key_Status.Key_Value0;
					}
			}
			else
			{
					Key_Status.Key_Value_Last=Key_Status.Key_Value0;
					Key_Status.Key_Long_Filter=0;
					Key_Status.Key_IN_Filter=0;
			}
		}
	}
	else
	{
			if(!Key_Status.Key_IN_Long)
			{
					if(Key_Status.Key_IN_Flag)
							Key_Status.Key_Value0 = Key_Status.Key_Value_Saved | 0x80;
			}
			Key_Status.Key_IN_Long=0;
			Key_Status.Key_Value_Last=0;
			Key_Status.Key_IN_Flag=0;
			Key_Status.Key_Long_Filter=0;
			Key_Status.Key_IN_Filter=0;
			Key_Status.Key_Value_Saved=0;
	}
	
	return Key_Status.Key_Value0;
}
#else
/*K4模式 —— 不开启扫描*/
u8 Key_Scan_Handler(void)
{
	Key_Status.Key_Value0 = 0;
		if((!Key_MOD) || (!Key_L2) || (!Key_L3) || (!Key_R1))
		{
				if(!Key_MOD)
					Key_Status.Key_Value0 |= 0x01;
				if(!Key_L2)
					Key_Status.Key_Value0 |= 0x02;
				if(!Key_L3)
					Key_Status.Key_Value0 |= 0x04;
				if(!Key_R1)
					Key_Status.Key_Value0 |= 0x08;
		}
		
		if(Key_Status.Key_Value0)
		{
				if(Key_Status.Key_Value0==Key_Status.Key_Value_Last)
				{
						if(!Key_Status.Key_IN_Flag)
						{
								if(++Key_Status.Key_IN_Filter>5)
								{
										Key_Status.Key_IN_Flag=1;
										Key_Status.Key_Value_Saved=Key_Status.Key_Value_Last;
								}
						}
						else
						{
								if(!Key_Status.Key_IN_Long)//?????,?????????,??????
								{
										if(++Key_Status.Key_Long_Filter>120)//????
										{
												Key_Status.Key_IN_Long=1;
												Key_Status.Key_Long_Filter=0;
												Key_Status.Key_Value0 = Key_Status.Key_Value_Last | 0XC0;
												return Key_Status.Key_Value0;
										}
								}
								else
										return Key_Status.Key_Value0;
						}
				}
				else
				{
						Key_Status.Key_Value_Last=Key_Status.Key_Value0;
						Key_Status.Key_Long_Filter=0;
						Key_Status.Key_IN_Filter=0;
				}
		}
		else
		{
				if(!Key_Status.Key_IN_Long)
				{
						if(Key_Status.Key_IN_Flag)
								Key_Status.Key_Value0 = Key_Status.Key_Value_Saved | 0x80;
				}
				Key_Status.Key_IN_Long=0;
				Key_Status.Key_Value_Last=0;
				Key_Status.Key_IN_Flag=0;
				Key_Status.Key_Long_Filter=0;
				Key_Status.Key_IN_Filter=0;
				Key_Status.Key_Value_Saved=0;
		}
		return Key_Status.Key_Value0;
}
#endif
/*
void Int_Sys_Values(void)
{
	Key_Status.Key_Value0 = 0;
	Key_Status.Key_SCAN_State=0;
	Key_R1=0;
	Key_Status.Key_Value_Last=0;
	Key_Status.Key_IN_Flag=0;
	Key_Status.Key_Long_Filter=0;
	Key_Status.Key_IN_Filter=0;
}*/


u8 SPEED_UNIT_CONVERT(u8 State , u8 Dat_IN)
{
	u16 Temp16;
	u8  Temp8;
	if(State)//				Km/h  --->   mph
	{
			Temp16 = (u16)((Dat_IN)*62/10);
			if(Temp16%10>4)
				Temp16 = Temp16/10+1;
			else
				Temp16 /= 10;
			Temp8 = (u8)Temp16;
	}
	else
	{
			Temp16 = (u16)((Dat_IN)*100/62);
			Temp8 = (u8)Temp16;
	}
	return Temp8;
}



