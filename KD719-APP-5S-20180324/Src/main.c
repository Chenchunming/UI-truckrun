/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
	1.KD719彩屏单色 5S协议新版界面 																			- 2018-04-11
	
	**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
	---------**  DEBUG:  **----------------
	1.ODO与TRIP相差10倍 （公制km下ODO未除10）					-	20180412
	
	Bin Head  01 45 02 00 00 00 00 00 00 00 00 00 00 00 00 00 
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "wwdg.h"
#include "gpio.h"

#include "Config(776Nxx01).h"

/* USER CODE BEGIN Includes */
#include "delay.h"
#include "lcd.h"
#include "sys.h"

#define Setting_Admin_PW_EN 0

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define THE_CODE_Length 8141

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
//RTC_TimeTypeDef stimestructure;
const u16 Wheel_Perimeter_Seach_Tab[8]={1277,1436,1596,1756,1915,2075,2154,2234};

uint8_t RX_Flag , USART1_RXTime_OUT;
uint8_t Rx_Len , RX_NUM;
uint8_t SYS_Unit_S369_Flag;
uint16_t Shut_SYS_Delay ;
uint16_t USB_Check_Delay , USB_Relase_Delay;

uint32_t ADC_Value[150];

uint16_t LIGHT_ON_SET = 4000;
uint16_t LIGHT_OFF_SET = 3980;

uint16_t LIGHT_SW_LEVE[5] = 
{
					3900,
					3950,
					4000,
					4020,
					4040
};

uint8_t AUTO_LIGHT_Sensitivity;

uint16_t AL_Standard_Value;

//uint32_t ODO_Test;

uint8_t  Timer_Hour , Timer_Minute , Timer_Second;
uint8_t  Timer_Second_Delay;

u8 Sys_New_Born_Flag;//系统刚烧完程序，参数需要初始化。初始化完成后，会将0XAA写入 数据存储区的 第一个字节 。所以，当读出的数据为0xaa时，则不需要初始化参数

/*配置表
1：公制/英制  单位选择
2：背光亮度 0-4 五级选择
3：自动关机时间 OFF、1-9分钟
4：电压/百分比
5：限流值
6：清零单次里程
7：轮径
8：系统电压
9：限速值
10:光敏灵敏度
11:速度传感器个数
12:助力传感器个数
13.档位设置
14.缓启动参数
*/
u8 Config_Temp[14]={0,5,5,0,1,0,28,0,25,3,6,12,2,2};
u8 Error_Code_Tab[11]={0,0,0,0,0,0,0,0,0,0};
u8 S_Num_Err;
u8 Bat_Message[26]={
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0};
u8 Bat_Valtages[42]={//支持最大20串
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0
};
u8 Bat_Index_Pages=0 , Bat_Index_Page_Reload=0 , Battey_Index_Waiting=0 , Bat_Sirl_Num;

u8 WALK_EXIT_DELAY;

u8 Send_Lenth=0;
//u8 Send_Mode=1,Send_Mode_S=1,Send_Mode_Deep=0;
u8 UART_Send_Settings_Delay;
u8 UART_Send_Settings_Flag;
u8 UART_Send_Allowed_Flag;
u8 Control_ANS_Flag;
//u8 Send_Uart_Delay,Send_UART_Settings_Delay;
u8 Call_Reply_Flag,Call_Reply_Mask;

u8 Moto_Working_Flag;

u16 Time_For_BF_ShutWithClear;
uint16_t AVG_Buf = 0;

u8  Key_Scan_Delay;
u16 Wheel_T , S_Wheel_T;
u8  SPD_ERR;
u8  PAS_Value,PAS_Value_Buffer,PAS_Top_Vf;

//u8 BF_PAS_STab_9D[9]={1,11,12,13,2,21,22,23,3};
//u8 BF_PAS_STab_5D[5]={11,13,21,23,3};
//u8 BF_PAS_STab_3D[3]={12,2,3};
u8  SCA_Value_tab_3D[3]={50,74,92};
u8  SCA_Value_tab_5D[5]={15,25,35,50,100};
u8  SCA_Value_tab_7D[7]={40,50,60,70,80,90,96};
u8  SCA_Value_tab_9D[9]={25,34,43,52,61,70,79,88,96};
u8 PAS_Temp6;
u8 *PAS_Tab_Set;
u8  P_Setting_Selc;
//u8 PAS_Send_Now_Mask;//PAS发生变化，立即发送PAS

u8 Trip_Record_Save_Delay;

u8 Sys_Code_Tab[4]={0,0,0,0};
u8 User_Input_Code_Tab[4]={0,0,0,0};
u8 User_Input_Code_Buf[4]={0,0,0,0};
u8 Code_Input_Flag;

const u8 Admin_PassW[4]={1,6,6,5};
const u8 Factry_PassW[4]={0,1,2,5};

u16 Wheel_Perimeter;
u8  Wheel_Dia_Mark;//轮径代码
u32 KM_Speed_Base_Value,MH_Speed_Base_Value,Speed_Base_Value;
u16 Speed_Buf,D_Speed00,D_Speed,CCS_Speed,CCS_Speed_Stro,D_Speed_Stro;
//u8 Speed_ReLoad_Delay;//,Speed_ReLoad_Delay_Top;

u8 Walk_PWM_Temp,Walk_Add_Delay;

u16 SYS_Voltage , SYS_Voltage_Fr , SYS_BAT_Flash;
u16 SYS_Voltage_Start,SYS_Voltage_Dec;
u8  Voltage_Check_SS,SYS_Voltage_DelayS;
u8  Voltage_DEC_Delay,Voltage_DEC_Base;
u8  LOW_Voltage_Flag;
u16 SYS_V_Stand_Value[5];
u8  V_Stand_Sel , V_Stand_Sel_SBuf;
u8  V_Setting_Sel;

u16 Light_ADCIN;

//u8  MAX_Limit_Speed;
u8  DL_Delay_Time;//电量延时 1-3S  2-6S  3-12S
u8  C_Current , Battery_SOC , Battery_SOC_FR , Battery_SOC_Start , Battery_SOC_Flit;
u8  V_Sys_Worked;
u16 V_Standard_Value,V_In_Value;
u16 Motor_Power_CC,Motor_Power_CC_Br;

u8  D500mS_Count;
u8  Send_Data[10];//发送缓存，最大十个字节

u16  Setting_Value_Temp,Setting_Value_Top_Limit,Setting_Value_Bottom_Limit;

u8 SYS_Pus_FR,SYS_Pus_SCN,SYS_Auto_OFF,SYS_P_V_Seclect,SYS_CUR_Limit_Value,PAS_CP_CGN;
u8 Speed_Limte_Code,Speed_Limte_BUF,Speed_Limte_MPH;
u16 Real_Speed_Limte_Value;

u8 Speed_Over_Limte_Check,Speed_Over_Limte_Flag,PAS_Value_Dec;

u8  Restore_Settings_Flag;
u8  Avg_JS_Flag , AVG_Dec_Delay;

u8 NoCommunication_OverStand;

u32 Sys_Auto_ShutUp_Delay;

uint8_t  PW_Send_Flag;

uint8_t  SYS_Pus_CPn , SYS_HAND_F_Flag , SYS_HAND_L_Flag , SYS_PUS_SSP;

uint8_t  PAS_Scroll_Step , PAS_Scroll_Delay;

uint8_t  PAS_V_Last;

struct//特殊模式相关标识EMC
{
	u16 Start_Delay:16;//开机3分钟计时
	u16 Check_Delay:16;//速度稳定计时
	u8  EN_Flag:1;//EMC模式标识
	u16 EMC_Sped_Buf:16;//EMC下速度记忆
	u8  EMC_Check_OK:1;//当前条件满足EMC测试条件
	u16  EMC_Exit_Delay:8;//退出检测
	u8  EMC_Exit_Delay2:8;//退出检测
}EMC_TEST;

struct//显示相关标识
{
	u8 Bat_ReLoad_Flag:1;//电池电量显示刷新
	u8 Bat_ShutDis_Flag:1;//电池符号关闭显示标识  用于 欠压闪烁
	u8 Spd_ReLoad_Flag:1;//速度显示刷新
	u8 PCC_ReLoad_Flag:1;//功率显示刷新
//	u8 TRIP_Dis_Flag:1;//TRIP显示刷新
	u8 PAS_ReLoad_Flag:1;
	u8 ERR_ReLoad_Flag:1;
	u8 TIME_ReLoad_Flag:1;
	u8 Light_ReLoad_Flag:1;
	u8 USB_ReLoad_Flag:1;
	u8 WALK_ReLoad_Flag:1;
	u8 RANGE_DIS_EN:1;
	u8 CALORIES_DIS_EN:1;
	u8 E_S_SW_Flag:1;
	u8 R_S_Flag:1;
	u8 C_S_Flag:1;
	u8 R_DIS_SDelay:8;//开机20秒内有答复
	u8 C_DIS_SDelay:8;//开机20秒内有答复
}LCD_DisPlay;

struct//设置模式相关变量、标识
{
//	unsigned char i_Key_IN:1;//    i键双击标识
//	unsigned char i_Key_ON:1;//    i键单击标识
//	unsigned char i_Key_DL:8;//    i键双击延时
	unsigned char Mode:8;//      菜单级别
	unsigned char Start_Code_Flag:1;//开机密码标识
	//unsigned char Level:8;//     子菜单序号
	unsigned char Selected:8;//  当前子项
	unsigned char Chosen:8;  //  选定子项
	unsigned char View_S:8;  //  选定子项
	unsigned char In_Flag:1;//   设置模式标识 
	unsigned char Exit_Flag:1;//   退出设置模式标识 
	unsigned char UpDat_Flag:1;//界面及初始化参数更新标识
	unsigned char Card_ReLoad_Flag:1;
	unsigned char Setting_ReLoad_Mask:1;//界面及初始化参数更新标识
	unsigned char Set_OK_View:1;//设置OK界面
	unsigned char Set_OK_View_Delay:8;//显示OK延时计数
	unsigned char SOC_Rev_Flag:1;//接收到控制器返回的SOC值了，如果此标识为0，则使用采集到的电压值显示电量
	unsigned char Restor_Now_Flag:1;//恢复出厂设置界面
	unsigned char Clrear_Trip_Flag:1;//清零单次里程操作
	unsigned char Save_PassWord_Flag:1;//清零单次里程操作
	unsigned char PassWord_Changed_Flag:1;
	unsigned char PAS_Val_Changed_Flag:1;
//	unsigned char Error_Code_Clear_Flag:1;//清理错误标识
	unsigned char Time_Need_To_Save_Flag:1;
	unsigned char PW_Last_States:1;//输入密码界面开启
	unsigned char Password_Set_States:8;//密码界面流程标识
	unsigned char PW_Set_MSG_Flag:8;
	unsigned char PW_Set_MSG_Delay:8;
	unsigned char Real_Time_Edit_Flag:1;
}Setting;

struct//工作模式、状态标识
{
  unsigned char Interface_ReLoad_Mask:1;//界面刷新标识
  unsigned char Working_Mode:8;//0-5  5种界面显示
  unsigned char P_Walk_Flag:1;//助力推行模式
  unsigned char Speed_Unit_GY_Flag:1;//速度单位转换标识
  unsigned char Power_Unit_PI_Flag:1;//功率单位转换标识
  unsigned char Light_Switch_Flag:1;//大灯开启关闭标识
  unsigned char BLK_Light_Level:8;//背光亮度级别
  unsigned char Current_Err_CODE:8;//错误代码
  unsigned char Current_Err_CODE_Bk:8;//错误代码
  unsigned char SYS_Err_Occurred_Flag:1;//错误产生标识
  unsigned char Moving_State_Chg:1;//运动状态发生改变
  unsigned char SYS_Power_Just_ON:1;//系统初始上电
  unsigned char SYS_Power_States_Flag:1;//系统初始上电标识
  unsigned char SYS_Start_ON_Flag:1;//完成开机标识
  unsigned int  Current_Speed:16;//当前车速
  unsigned char SCA_Leve_Setting:8;//助力档位
  unsigned char SYS_Setting_OverFlow:8;//系统设置超时计数
  unsigned char NoCommunication_OverFlow:8;//通讯超时计数
  unsigned char SYS_CodeIN_Setting:8;//密码设置
  unsigned char SYS_Settint_Reset_Flag:1;
  unsigned char Call_IN_From_PC_Flag:1;//PC端呼叫标识
  unsigned char SYS_Start_PassWord_EnAble:1;//需要开机密码输入标识
	unsigned char SYS_Start_PassWord_SBuf:1;//需要开机密码输入标识
  unsigned char Bike_Moving_Flag:1;//速度为0标识
  unsigned char MAX_SPD_Reset:1;//存储最大速度标识
  unsigned char ShutDown_Flag:1;//关机命令标识
  unsigned char See_Volatge_Flag:1;//计算电压命令
  unsigned char Save_Datas_Flag:1;//保存数据命令
  unsigned char USB_State_Flag:1;
  unsigned char USB_Sleep_Flag:1;
  unsigned char Light_Sensing_Flag:1;//光感状态标识
  unsigned char Light_Sense_Filter:8;//光感状态滤波
  unsigned char Light_Sensing_EN:1;//光感控制有效标识，开机有效，手动操作后失效
//	unsigned char Engineering_Mode_MASK:1;//工程模式标识，该模式下，不记忆错误代码
}Work_Status;

struct//里程计算相关变量
{
  u16  Base_Time_Count_Delay:16;
  u16  Base_Time_Count_NUM:16;
  u32  Base_mm_Count:32;//单位mm
  u16  Trip_Time_Delay:16;
}Mileage_Variable;

struct//里程数据
{
  u32  Total_Trap_Record:32;//0-999  总里程
  u32  Total_Trap_Record_Mile:32;//0-999  总里程(英制)
//  u16  Service_Milage:16; //售后里程记录
  u16  Service_Milage_ADD:8; //售后里程记录
  u16  Single_Trap_At_Once:16; //单次里程记录
  u16  Single_Trap_At_Once_Mile:16;//单次里程记录(英制)
  u16  Timer_Of_This_Trap:16;   //运动时间记录
	u16  Second_Of_This_Trap:16;
  u16  MAX_Speed_In_This_Trap:16;//最高速度
  u16  MAX_Speed_In_This_Trap_Mile:16;//最高速度(英制)
  u16  AVG_Speed_In_This_Trap:16;//平均速度
  u16  AVG_Speed_In_This_Trap_Mile:16;//平均速度(英制)
  u16  Trap_Range:16;//剩余里程
  u16  Trap_Range_Fr:16;//剩余里程
  u16  S_Calorie:16;//卡路里
  u16  S_Calorie_Fr:16;//卡路里
}Sport_Recording;

struct//串口数据收发相关标识
{
	//u8 Need_Rev_Data_Mask:1;//标识需要串口回复数据
	//u8 RevBack_Time_Out:8;//等待回复超时计时
	//u8 RevBack_Time_Out_Flag:1;//接收超时标识
	//u8 Just_Send_Flag:1;//数据发送标识
	u8 Send_Time_Delay:8;//数据发送最小间隔
	u8 Send_Scenes_Flag:1;//发送工作模式
	u8 Send_Scenes_Delay:8;//发送延时、间隔
	u8 Send_Scenes_TimeOut:8;//发送超时命令
	u8 REV_Back_OUT_Time:8;//接收超时计时，超时后复位串口
	
	u8 Send_States_Flag:1;//数据发送状态，5S协议下定义：0-未启动发送，1-发送系统参数设置命令，2-发送运行数据
	u8 USART_Send_EN:1;
}MY_USART;
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
#ifdef Start_LOGO_EN
void Dis_LOGO(void)
{
	uint16_t i,j,t,k,S_x,S_y;
	uint8_t BS;
    
	S_x = (320-LOGO_W)/2;
	S_y = ((480-LOGO_H)/2) + LOGO_H - 50;

	for(t=0;t<50;t++)
	{
		for(i=0;i<LOGO_H;i++)
		{
			LCD_SetXY(S_x , S_y+t-i , LOGO_W);
			for(j=0;j<LOGO_W/8;j++){
                BS = Client_LOGO[(i*LOGO_W/8) + j];
                for(k=0;k<8;k++)
                {
                    if(BS&0x80)
                        LCD_WriteData(Def_BGC);
                    else
                        LCD_WriteData(Def_FGC);
                    BS <<= 1;
                }
            }
		}
		LCD_SetXY(S_x , S_y+t-LOGO_H-1 , LOGO_W);
		for(j=0;j<LOGO_W;j++)
			LCD_WriteData(Def_BGC);
		delay_ms(7);
	}
	delay_ms(1000);
}
#endif
/*开机MCU初始化*/
void Start_MCU(void)
{/* MCU Configuration----------------------------------------------------------*/
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_ADC1_Init();
//	MX_RTC_Init();
	MX_WWDG_Init();
	delay_init(120);
	USART1_RXTime_OUT = 2;
	MX_USART1_UART_Init(9600);
	Int_Uart_DMA_Receive();//开启串口接收
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&ADC_Value, 150);/*-1- Start the DMA - ADC*/
	HAL_TIM_Base_Start_IT(&htim2);/*-2- Start the TIMER*/
	HAL_WWDG_Start_IT(&hwwdg);	/*-3- Start the WWDG*/
	delay_ms(5);
}
/*设置分段电压*/
void Set_Valtage_Segment(void)
{
	if(V_Stand_Sel==1)
	{
		V_Sys_Worked = 2;
		SYS_V_Stand_Value[0]=SET_36V_2_V1;
		SYS_V_Stand_Value[1]=SET_36V_2_V2;
		SYS_V_Stand_Value[2]=SET_36V_2_V3;
		SYS_V_Stand_Value[3]=SET_36V_2_V4;
		SYS_V_Stand_Value[4]=SET_36V_2_V5;
	}
	else if(V_Stand_Sel==2)
	{
		V_Sys_Worked = 2;
		SYS_V_Stand_Value[0]=SET_36V_3_V1;
		SYS_V_Stand_Value[1]=SET_36V_3_V2;
		SYS_V_Stand_Value[2]=SET_36V_3_V3;
		SYS_V_Stand_Value[3]=SET_36V_3_V4;
		SYS_V_Stand_Value[4]=SET_36V_3_V5;
	}
	else if(V_Stand_Sel==3)
	{
		V_Sys_Worked = 2;
		SYS_V_Stand_Value[0]=SET_36V_4_V1;
		SYS_V_Stand_Value[1]=SET_36V_4_V2;
		SYS_V_Stand_Value[2]=SET_36V_4_V3;
		SYS_V_Stand_Value[3]=SET_36V_4_V4;
		SYS_V_Stand_Value[4]=SET_36V_4_V5;
	}
	else if(V_Stand_Sel==4)
	{
		V_Sys_Worked = 3;
		SYS_V_Stand_Value[0]=SET_48V_5_V1;
		SYS_V_Stand_Value[1]=SET_48V_5_V2;
		SYS_V_Stand_Value[2]=SET_48V_5_V3;
		SYS_V_Stand_Value[3]=SET_48V_5_V4;
		SYS_V_Stand_Value[4]=SET_48V_5_V5;
	}
	else if(V_Stand_Sel==5)
	{
		V_Sys_Worked = 3;
		SYS_V_Stand_Value[0]=SET_48V_6_V1;
		SYS_V_Stand_Value[1]=SET_48V_6_V2;
		SYS_V_Stand_Value[2]=SET_48V_6_V3;
		SYS_V_Stand_Value[3]=SET_48V_6_V4;
		SYS_V_Stand_Value[4]=SET_48V_6_V5;
	}
	else
	{
		V_Sys_Worked = 2;
		SYS_V_Stand_Value[0]=SET_36V_1_V1;
		SYS_V_Stand_Value[1]=SET_36V_1_V2;
		SYS_V_Stand_Value[2]=SET_36V_1_V3;
		SYS_V_Stand_Value[3]=SET_36V_1_V4;
		SYS_V_Stand_Value[4]=SET_36V_1_V5;
	}
}
//设置档位
void Set_Power_PAS_DW(void)
{
	PAS_Temp6=Work_Status.SCA_Leve_Setting/2*2+3;
	if(PAS_Temp6==3)PAS_Tab_Set=&SCA_Value_tab_3D[0];
	if(PAS_Temp6==5)PAS_Tab_Set=&SCA_Value_tab_5D[0];
	if(PAS_Temp6==7)PAS_Tab_Set=&SCA_Value_tab_7D[0];
	if(PAS_Temp6==9)PAS_Tab_Set=&SCA_Value_tab_9D[0];
	PAS_Top_Vf=PAS_Temp6;
	if(PAS_Value>PAS_Top_Vf)
		PAS_Value=PAS_Top_Vf;
}
/*开机参数初始化*/
void Initialization_System_Start(void)
{
	ADC_Value[147] = 0;
	Work_Status.SYS_Start_ON_Flag = 0;
	Work_Status.SYS_Power_States_Flag = 0;
	Work_Status.SYS_Power_Just_ON=1;
	Sport_Recording.Trap_Range=0;
	Work_Status.Interface_ReLoad_Mask=0;
	PAS_Value=DEFAULT_START_PAS;
	Setting.Start_Code_Flag = 0;
	Work_Status.USB_Sleep_Flag = 0;
//	V_Sys_Worked=SYSTEM_DEFAULT_VOLTAGE;
	Setting.SOC_Rev_Flag = 0;
	Work_Status.Call_IN_From_PC_Flag=0;
	Mileage_Variable.Base_Time_Count_Delay=60000;
	Sys_Auto_ShutUp_Delay=0;
	Restore_Settings_Flag=0;
	Sport_Recording.S_Calorie=0;
	if(Sport_Recording.Timer_Of_This_Trap>0 || Sport_Recording.Second_Of_This_Trap>0)
		Sport_Recording.AVG_Speed_In_This_Trap=(Sport_Recording.Single_Trap_At_Once*3600)/((Sport_Recording.Timer_Of_This_Trap*60) + Timer_Second + Sport_Recording.Second_Of_This_Trap);
	else
		Sport_Recording.AVG_Speed_In_This_Trap=0;
	
	if(V_Stand_Sel>3)
		V_Sys_Worked = 3;
	else
		V_Sys_Worked = 2;
	
	Set_Power_PAS_DW();
	Time_For_BF_ShutWithClear = 0;
	
	DL_Delay_Time = 3;
	if(DL_Delay_Time==3)
		SYS_Voltage_DelayS=12;
	else
		SYS_Voltage_DelayS=DL_Delay_Time*3;
	
	SYS_Voltage_Start=0;
	Voltage_DEC_Base=50;
//	if(Wheel_Dia_Mark==27)
//		 2193;
//	else
//		Wheel_Perimeter = (u32)(Wheel_Dia_Mark*79756/1000);
	Wheel_Perimeter = Wheel_Perimeter_Seach_Tab[Wheel_Dia_Mark];
	KM_Speed_Base_Value=Wheel_Perimeter*36;
	MH_Speed_Base_Value=Wheel_Perimeter*360/16;
	
	if(Work_Status.Speed_Unit_GY_Flag)
		Speed_Base_Value=MH_Speed_Base_Value;
	else
		Speed_Base_Value=KM_Speed_Base_Value;
	
	
	EMC_TEST.Check_Delay = 0;
	NoCommunication_OverStand = Err_30_Time_Delay;
	Work_Status.Light_Sensing_Flag = 0;
	Setting.In_Flag=0;
	Setting.Mode=0;
	Rx_Len = 0;
	Rx_Cnt = 0;
	Rx_Time_Over = 0;
	RX_Flag = 0;	
	Battery_SOC_Flit = 0;
	LCD_DisPlay.RANGE_DIS_EN = 1;
	LCD_DisPlay.CALORIES_DIS_EN = 1;
	LCD_DisPlay.R_S_Flag = 1;
	LCD_DisPlay.C_S_Flag = 1;
	
	if(Work_Status.Speed_Unit_GY_Flag)
		Real_Speed_Limte_Value = (u16)((Speed_Limte_Code+10)*63/10);
	else
		Real_Speed_Limte_Value = (u16)((Speed_Limte_Code+10)*10);
	
	if(AUTO_LIGHT_Sensitivity!=0)
	{
		Work_Status.Light_Sensing_EN = 1;
		if(AUTO_LIGHT_Sensitivity==5)
				LIGHT_ON_SET = AL_Standard_Value-160;
		else if(AUTO_LIGHT_Sensitivity==4)
				LIGHT_ON_SET = AL_Standard_Value-120;
		else if(AUTO_LIGHT_Sensitivity==3)
				LIGHT_ON_SET = AL_Standard_Value-80;
		else if(AUTO_LIGHT_Sensitivity==2)
				LIGHT_ON_SET = AL_Standard_Value-60;
		else if(AUTO_LIGHT_Sensitivity==1)
				LIGHT_ON_SET = AL_Standard_Value-40;
		LIGHT_OFF_SET = (LIGHT_ON_SET-20);
	}
	else
		Work_Status.Light_Sensing_EN = 0;
}
/*系统参数初始化*/
void Initialise_SYS_Parameter_Value(void)
{u8 i;
	/*开机密码不使能 ， 默认密码 0000*/
	Sys_Code_Tab[0]=0;
	Sys_Code_Tab[1]=0;
	Sys_Code_Tab[2]=0;
	Sys_Code_Tab[3]=0;
	Work_Status.SYS_Start_PassWord_EnAble=0;
	/*助力档位 默认 0-5*/
	Work_Status.SCA_Leve_Setting=DEFAULT_SCA_LEVEL;
	/*背光亮度*/
	Work_Status.BLK_Light_Level=DEFAULT_BACK_LIGHT;
	/*轮径代码*/
	//Wheel_Dia_Mark = DEFAULT_WHEEL_DIAMETER;
	if(DEFAULT_WHEEL_DIAMETER<27)
		Wheel_Dia_Mark=(DEFAULT_WHEEL_DIAMETER/2)-8;
	else if(DEFAULT_WHEEL_DIAMETER==27)
		Wheel_Dia_Mark=6;
	else
		Wheel_Dia_Mark=7;
	/*限流值*/
	SYS_CUR_Limit_Value=DEFAULT_CURRENT_LIMTE;
	/*限速值*/
	Speed_Limte_Code=DEFAULT_SPEED_LIMTE-10;//实际限速值 = 15+10
	/*速度传感器磁钢数*/
	SYS_Pus_CPn=DEFAULT_Pus_CPn;
	/*自动关机时间设定*/
	SYS_Auto_OFF=DEFAULT_AUTO_OFF_TIME;
	/*助力传感器方向*/
	SYS_Pus_FR=0;
	/*助力传感器灵敏度  -------->更改为光感灵敏度  */
	SYS_Pus_SCN=2;
	/*助力传感器磁盘磁钢数*/
	PAS_CP_CGN=DEFAULT_PAS_CP_C;
	/*电压显示设置：电压值 / 百分比*/
	SYS_P_V_Seclect=DEFAULT_VS_Select;
	/*缓启动参数*/
	SYS_PUS_SSP = DEFAULT_PUS_SSP;
	/*最大限速值*/
//	MAX_Limit_Speed=40;
	/*公制、英制切换标识*/
	Work_Status.Speed_Unit_GY_Flag=DEFAULT_UNIT_MASK;
	/*电压基准值*/
	if((V_Standard_Value < 100)||(V_Standard_Value > 1000))
		V_Standard_Value = 234;
	/*光感基准值*/
	if((AL_Standard_Value > 4094)||(AL_Standard_Value<3000))
		AL_Standard_Value = 4000;
	/*光感灵敏度*/
	AUTO_LIGHT_Sensitivity = DEFAULT_AUTO_LIGHT_LEVEL;
	
	Sport_Recording.Single_Trap_At_Once=0;
	Sport_Recording.Timer_Of_This_Trap=0;
	Sport_Recording.MAX_Speed_In_This_Trap=0;
	Sport_Recording.AVG_Speed_In_This_Trap = 0;
	/*清理错误代码*/
	for(i=0;i<11;i++)
		Error_Code_Tab[i]=0;
	
	V_Stand_Sel = DEFAULT_Stand_Sel;
	
	SCA_Value_tab_3D[0]=Deft_SCA_3D_1;
	SCA_Value_tab_3D[1]=Deft_SCA_3D_2;
	SCA_Value_tab_3D[2]=Deft_SCA_3D_3;

	SCA_Value_tab_5D[0]=Deft_SCA_5D_1;
	SCA_Value_tab_5D[1]=Deft_SCA_5D_2;
	SCA_Value_tab_5D[2]=Deft_SCA_5D_3;
	SCA_Value_tab_5D[3]=Deft_SCA_5D_4;
	SCA_Value_tab_5D[4]=Deft_SCA_5D_5;
	
	SCA_Value_tab_7D[0]=Deft_SCA_7D_1;
	SCA_Value_tab_7D[1]=Deft_SCA_7D_2;
	SCA_Value_tab_7D[2]=Deft_SCA_7D_3;
	SCA_Value_tab_7D[3]=Deft_SCA_7D_4;
	SCA_Value_tab_7D[4]=Deft_SCA_7D_5;
	SCA_Value_tab_7D[5]=Deft_SCA_7D_6;
	SCA_Value_tab_7D[6]=Deft_SCA_7D_7;
	
	SCA_Value_tab_9D[0]=Deft_SCA_9D_1;
	SCA_Value_tab_9D[1]=Deft_SCA_9D_2;
	SCA_Value_tab_9D[2]=Deft_SCA_9D_3;
	SCA_Value_tab_9D[3]=Deft_SCA_9D_4;
	SCA_Value_tab_9D[4]=Deft_SCA_9D_5;
	SCA_Value_tab_9D[5]=Deft_SCA_9D_6;
	SCA_Value_tab_9D[6]=Deft_SCA_9D_7;
	SCA_Value_tab_9D[7]=Deft_SCA_9D_8;
	SCA_Value_tab_9D[8]=Deft_SCA_9D_9;
}


void Set_Walk_KEY_IN(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}
/*EEPROM 数据 读写 —— 各档位助力百分比*/
void Read_Save_PAS_Value(u8 States)
{
	u8 i;
	u8 REP_Buf[24];
	if(States)//读档位助力百分比
	{
		AT24CXX_Read(41,REP_Buf,24);
		for(i=0;i<3;i++)
			SCA_Value_tab_3D[i]=REP_Buf[i];
		for(i=0;i<5;i++)
			SCA_Value_tab_5D[i]=REP_Buf[3+i];
		for(i=0;i<7;i++)
			SCA_Value_tab_7D[i]=REP_Buf[8+i];
		for(i=0;i<9;i++)
			SCA_Value_tab_9D[i]=REP_Buf[15+i];
	}
	else//写档位助力百分比
	{
		for(i=0;i<3;i++)
			REP_Buf[i]=SCA_Value_tab_3D[i];
		for(i=0;i<5;i++)
			REP_Buf[3+i]=SCA_Value_tab_5D[i];
		for(i=0;i<7;i++)
			REP_Buf[8+i]=SCA_Value_tab_7D[i];
		for(i=0;i<9;i++)
			REP_Buf[15+i]=SCA_Value_tab_9D[i];
		AT24CXX_Write(41,REP_Buf,24);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 电压分段*/
void Read_Save_5SEG_Valtage(u8 States)
{
	u8 i , WREP_Buf[10];
	if(States)//读电压分段
	{
		AT24CXX_Read(65,WREP_Buf,10);
		for(i=0;i<5;i++)
			SYS_V_Stand_Value[i]=(u16)((WREP_Buf[2*i]*256) + WREP_Buf[(2*i)+1]);
	}
	else//写电压分段
	{
		for(i=0;i<5;i++)
		{
			WREP_Buf[2*i] = SYS_V_Stand_Value[i]/256;
			WREP_Buf[(2*i)+1] = SYS_V_Stand_Value[i]%256;
		}
		AT24CXX_Write(65,WREP_Buf,10);
	}
}

/*EEPROM 数据 读写 —— 单次里程/时间*/
void Read_Save_Single_Trap(u8 States)
{
	if(States)//读单次里程数据
	{
		Sport_Recording.Single_Trap_At_Once=AT24CXX_ReadLenByte(25,2);
		Sport_Recording.Timer_Of_This_Trap=AT24CXX_ReadLenByte(27,2);
		Sport_Recording.Second_Of_This_Trap = AT24CXX_ReadLenByte(33,2);
	}
	else//写单次里程数据
	{
		AT24CXX_WriteLenByte(25,Sport_Recording.Single_Trap_At_Once,2);
		delay_ms(5);
		AT24CXX_WriteLenByte(27,Sport_Recording.Timer_Of_This_Trap,2);
		delay_ms(5);
		AT24CXX_WriteLenByte(33, Timer_Second ,2);
		delay_ms(5);
	}
}
/*EEPROM 数据 写 —— 总里程备份*/
void Save_Total_Trap_Bak(void)
{
	if(Sport_Recording.Total_Trap_Record<10000000)
	{
		AT24CXX_WriteLenByte(29,Sport_Recording.Total_Trap_Record,4);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 总里程*/
void Read_Save_Total_Trap_Record(u8 States)
{
	if(States)//读总里程数据   1677721
	{
		Sport_Recording.Total_Trap_Record = AT24CXX_ReadLenByte(21,4) & 0X00FFFFFF;
		if(Sport_Recording.Total_Trap_Record>10000000)//数据有误，读取备份数据
		{
			Sport_Recording.Total_Trap_Record = AT24CXX_ReadLenByte(29,4) & 0X00FFFFFF;
			if(Sport_Recording.Total_Trap_Record>10000000)//备份数据也乱了
			{
				Sport_Recording.Total_Trap_Record = 0;//清零ODO
				AT24CXX_WriteLenByte(21,0,4);
				delay_ms(5);
				Save_Total_Trap_Bak();
				Sport_Recording.Single_Trap_At_Once = 0;//清零trip time
				Sport_Recording.Timer_Of_This_Trap = 0;
				Read_Save_Single_Trap(0);
				
			}
		}
	}
	else//写总里程数据
		AT24CXX_WriteLenByte(21,Sport_Recording.Total_Trap_Record,4);
}

/*EEPROM 数据 读写 —— 最高速度*/
void Read_Save_MAX_Trap(u8 States)
{
	if(States)//读最大速度数据
	{
		Sport_Recording.MAX_Speed_In_This_Trap=AT24CXX_ReadLenByte(35,2);
		if(Sport_Recording.MAX_Speed_In_This_Trap>999)
		{
			Sport_Recording.MAX_Speed_In_This_Trap=0;
		}
	}
	else//写最大速度数据
	{
		AT24CXX_WriteLenByte(35,Sport_Recording.MAX_Speed_In_This_Trap,2);
		delay_ms(5);
	}
}

/*EEPROM 数据 读写 —— 开机密码*/
void Read_Save_SYS_PassWord(u8 States)
{
	u8 i,Trans_Buf[5];
	if(States)
	{
		AT24CXX_Read(1,Trans_Buf,5);
		for(i=0;i<4;i++)
			Sys_Code_Tab[i]=Trans_Buf[i];
		if(Trans_Buf[4]==0xaa)
			Work_Status.SYS_Start_PassWord_EnAble=1;
		else
			Work_Status.SYS_Start_PassWord_EnAble=0;
	}
	else
	{
		for(i=0;i<4;i++)
			Trans_Buf[i]=Sys_Code_Tab[i];
		if(Work_Status.SYS_Start_PassWord_EnAble)
			Trans_Buf[4]=0xaa;
		else
			Trans_Buf[4]=0;
		AT24CXX_Write(1,Trans_Buf,5);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 设置参数*/
void Read_Save_SYS_Setting_Parameter(u8 States)
{
	u8 Trans_Buf[15];
	if(States)
	{
		AT24CXX_Read(6,Trans_Buf,15);
		Work_Status.SCA_Leve_Setting = Trans_Buf[0];
		Work_Status.BLK_Light_Level = Trans_Buf[1];
		Wheel_Dia_Mark = Trans_Buf[2];
		SYS_CUR_Limit_Value = Trans_Buf[3];
		Speed_Limte_Code = Trans_Buf[4];
		SYS_Pus_CPn = Trans_Buf[5];//速度传感器磁钢数
		V_Stand_Sel = Trans_Buf[6];
		SYS_Auto_OFF = Trans_Buf[7];
		SYS_Pus_FR = Trans_Buf[8];
		SYS_Pus_SCN = Trans_Buf[9];
		PAS_CP_CGN = Trans_Buf[10];
		SYS_P_V_Seclect = Trans_Buf[11];
//		DL_Delay_Time = Trans_Buf[12];
		SYS_PUS_SSP = Trans_Buf[12];
		AUTO_LIGHT_Sensitivity = Trans_Buf[13];
		Work_Status.Speed_Unit_GY_Flag = Trans_Buf[14];
	}
	else
	{
		Trans_Buf[0] = Work_Status.SCA_Leve_Setting;
		Trans_Buf[1] = Work_Status.BLK_Light_Level;
		Trans_Buf[2] = Wheel_Dia_Mark;
		Trans_Buf[3] = SYS_CUR_Limit_Value;
		Trans_Buf[4] = Speed_Limte_Code;
		Trans_Buf[5] = SYS_Pus_CPn;//速度传感器磁钢数
		Trans_Buf[6] = V_Stand_Sel;
		Trans_Buf[7] = SYS_Auto_OFF;
		Trans_Buf[8] = SYS_Pus_FR;
		Trans_Buf[9] = SYS_Pus_SCN;
		Trans_Buf[10] = PAS_CP_CGN;
		Trans_Buf[11] = SYS_P_V_Seclect;
		Trans_Buf[12] = SYS_PUS_SSP;
		Trans_Buf[13] = AUTO_LIGHT_Sensitivity;
		Trans_Buf[14] = Work_Status.Speed_Unit_GY_Flag;
		AT24CXX_Write(6,Trans_Buf,15);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 电压基准值*/
void Read_Save_VStandard(u8 States)
{
	if(States)
	{
		V_Standard_Value = (u16)AT24CXX_ReadLenByte(37,2);
		delay_ms(5);
	}
	else
	{
		AT24CXX_WriteLenByte(37 , (u32)V_Standard_Value , 2);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 光感基准值*/
void Read_Save_AL_Standard(u8 States)
{
	if(States)
	{
		AL_Standard_Value = (u16)AT24CXX_ReadLenByte(39,2);
		delay_ms(5);
	}
	else
	{
		AT24CXX_WriteLenByte(39 , (u32)AL_Standard_Value , 2);
		delay_ms(5);
	}
}
/*参数、数据整体读写*/
void SaveAndRead_Data_By_aBigTab(u8 States)
{
	if(States==0x87)
	{
		Sys_New_Born_Flag=AT24CXX_ReadOneByte(0);
		if(Sys_New_Born_Flag==0xaa)
		{
			Read_Save_Total_Trap_Record(1);
			Read_Save_Single_Trap(1);
			Read_Save_MAX_Trap(1);
			Read_Save_SYS_PassWord(1);
			Read_Save_SYS_Setting_Parameter(1);
			//Read_Save_SCA_Value(1);
			Read_Save_VStandard(1);
			Read_Save_AL_Standard(1);
			Read_Save_5SEG_Valtage(1);
		}
		else//系统第一次上电初始化
		{
			Sys_New_Born_Flag=0xaa;
			AT24CXX_WriteOneByte(0,0xaa);
			Read_Save_Total_Trap_Record(1);
			Initialise_SYS_Parameter_Value();
			Set_Valtage_Segment();//设置分段电压
			States=0x25;
		}
	}
	if(States==0x25)
	{
//		INTX_DISABLE();//关闭所有中断
		Read_Save_5SEG_Valtage(0);
		Read_Save_SYS_Setting_Parameter(0);
		Read_Save_Total_Trap_Record(0);
		Read_Save_Single_Trap(0);
		Read_Save_MAX_Trap(0);
		Read_Save_SYS_PassWord(0);
		Read_Save_PAS_Value(0);
		Read_Save_VStandard(0);
//		INTX_ENABLE();//开启所有中断
	}
}

/*初始化主页面显示*/
void Int_Index_DisPlay(void)
{
	LCD_Fill_Back_Ground(Work_Status.Speed_Unit_GY_Flag);
	LCD_DisPlay.PCC_ReLoad_Flag=1;
	LCD_DisPlay.Bat_ReLoad_Flag=1;
	LCD_DisPlay.Spd_ReLoad_Flag=1;
//	Dis_The_Big_Point(1);
	LCD_DisPlay.PAS_ReLoad_Flag=1;
	Work_Status.Interface_ReLoad_Mask=1;
	LCD_DisPlay.TIME_ReLoad_Flag = 1;
	LCD_DisPlay.USB_ReLoad_Flag = 1;
	LCD_DisPlay.Light_ReLoad_Flag = 1;
}

/*速度显示平滑处理*/
u16 Speed_Smooth_BM(u16 Ispeed)
{
	if(Speed_Buf>Ispeed)
	{
		if((Speed_Buf-Ispeed)>5)
			Speed_Buf-=3;
		else
			Speed_Buf--;
	}
	else
	{
		if(Speed_Buf<Ispeed)
		{
			if((Ispeed-Speed_Buf)>5)
				Speed_Buf+=3;
			else
				Speed_Buf++;
		}
	}
	return Speed_Buf;
}
//PAS档位切换动画效果
void PAS_Value_Scroll_Display(void)
{
	switch(PAS_Scroll_Step)
	{
		case 5:
			Clr_PAS_DisArea(0);
			Clr_PAS_DisArea(1);
			Clr_PAS_DisArea(2);
			Clr_PAS_DisArea(3);
			Clr_PAS_DisArea(4);
			Clr_PAS_DisArea(5);
			DisPlay_PAS_Numx_Zoom(0,1);
			DisPlay_PAS_Numx_Zoom(1,1);
			DisPlay_PAS_Numx_Zoom(2,1);
			DisPlay_PAS_Numx_Zoom(3,1);
			DisPlay_PAS_Numx_Zoom(4,1);
			DisPlay_PAS_Numx_Zoom(5,1);
			PAS_Scroll_Step = 1;
			PAS_V_Last = PAS_Value;
		case 4:
			Clr_PAS_DisArea(PAS_V_Last);
			if(PAS_V_Last>0)
				DisPlay_PAS_Numx_Zoom(PAS_V_Last-1,1);
			DisPlay_PAS_Numx_Zoom(PAS_V_Last,1);
			if(PAS_V_Last<5)
				DisPlay_PAS_Numx_Zoom(PAS_V_Last+1,1);
		break;
		case 3:
			DisPlay_PAS_Numx_Zoom(PAS_Value,2);
		break;
		case 2:
			DisPlay_PAS_Numx_Zoom(PAS_Value,4);
		break;
		case 1:
			DisPlay_PAS_Numx_Zoom(PAS_Value,8);
		break;
		case 0:
			DisPlay_PAS_Numx_Zoom(PAS_Value,0);
			PAS_V_Last = PAS_Value;
		break;
	}
}
/*显示各个分解项目*/
void LCD_DisPlay_ReLoad(void)
{
	u8 v_i;
	if(Avg_JS_Flag)
	{
		Avg_JS_Flag = 0;
		if(Sport_Recording.Timer_Of_This_Trap>0 || Timer_Second>0)
			Sport_Recording.AVG_Speed_In_This_Trap=(Sport_Recording.Single_Trap_At_Once*3600)/((Sport_Recording.Timer_Of_This_Trap*60) + Timer_Second + Sport_Recording.Second_Of_This_Trap);
		else
			Sport_Recording.AVG_Speed_In_This_Trap=0;
		if(AVG_Buf!=Sport_Recording.AVG_Speed_In_This_Trap)
		{
			AVG_Buf = Sport_Recording.AVG_Speed_In_This_Trap;
			if(Work_Status.Working_Mode == 3)
				Work_Status.Interface_ReLoad_Mask = 1;
		}
	}
	if(Setting.Exit_Flag)
	{
		Int_Index_DisPlay();/*页面显示初始化*/
		Setting.Exit_Flag=0;
		Work_Status.Interface_ReLoad_Mask=1;
		Wheel_Perimeter = Wheel_Perimeter_Seach_Tab[Wheel_Dia_Mark];
		KM_Speed_Base_Value = Wheel_Perimeter*36;
		MH_Speed_Base_Value = Wheel_Perimeter*360/16;
		if(Work_Status.Speed_Unit_GY_Flag)
			Speed_Base_Value=MH_Speed_Base_Value;
		else
			Speed_Base_Value=KM_Speed_Base_Value;
		if(Work_Status.SYS_Err_Occurred_Flag)
			LCD_DisPlay.ERR_ReLoad_Flag=1;
		if(Work_Status.Light_Switch_Flag)
			LCD_DisPlay.Light_ReLoad_Flag = 1;
		if(Work_Status.USB_State_Flag)
			LCD_DisPlay.USB_ReLoad_Flag = 1;
		if(Work_Status.Speed_Unit_GY_Flag)
			Real_Speed_Limte_Value = (u16)((Speed_Limte_Code+10)*63/10);
		else
			Real_Speed_Limte_Value = (u16)((Speed_Limte_Code+10)*10);
	}
	if(Work_Status.Interface_ReLoad_Mask && Work_Status.SYS_Err_Occurred_Flag==0)
	{
		switch(Work_Status.Working_Mode)
		{
			case 0:
			case 1:
	            Clr_TFTLCD_Area(98,83,162,104,Truckrun_white);
	            Clr_TFTLCD_Area(98,46,162,67,Truckrun_white);
	            Clr_TFTLCD_Area(257,64,320,88,Truckrun_white);
	            Clr_TFTLCD_Area(200,102,315,134,Truckrun_white);
	            Clr_TFTLCD_Area(200,19,315,51,Truckrun_white);
                Dis_Graphblock_bicolor(98,104,61,21,Truckrun_white,Truckrun_blue,word_trip,168);
                Dis_Graphblock_bicolor(98,67,61,21,Truckrun_white,Truckrun_blue,word_odo,168);
				if(Work_Status.Speed_Unit_GY_Flag)
				{
					Dis_Play_L16Data_H24(200,102,Truckrun_blue,Truckrun_white,0,1,Sport_Recording.Single_Trap_At_Once*62/100);
					Dis_Play_L16Data_H24(200,19,Truckrun_blue,Truckrun_white,0,0,(u32)(Sport_Recording.Total_Trap_Record*62/1000));
                    Show_Word_Agency(257,63,0,"mile",Truckrun_blue,Truckrun_white);
				}
				else
				{
					Dis_Play_L16Data_H24(200,102,Truckrun_blue,Truckrun_white,0,1,Sport_Recording.Single_Trap_At_Once);
					Dis_Play_L16Data_H24(200,19,Truckrun_blue,Truckrun_white,0,0,(u32)(Sport_Recording.Total_Trap_Record/10));
					Show_Word_Agency(257,63,0,"KM",Truckrun_blue,Truckrun_white);
				}
                break;
			case 2:
			case 3:
	            Clr_TFTLCD_Area(98,83,162,104,Truckrun_white);
	            Clr_TFTLCD_Area(98,46,162,67,Truckrun_white);
	            Clr_TFTLCD_Area(257,64,320,88,Truckrun_white);
	            Clr_TFTLCD_Area(200,102,315,134,Truckrun_white);
	            Clr_TFTLCD_Area(200,19,315,51,Truckrun_white);
                Dis_Graphblock_bicolor(98,104,58,21,Truckrun_white,Truckrun_blue,word_max,168);
                Dis_Graphblock_bicolor(98,67,55,21,Truckrun_white,Truckrun_blue,word_avg,147);
				if(Work_Status.Speed_Unit_GY_Flag)
				{
					Dis_Play_L16Data_H24(200,102,Truckrun_blue,Truckrun_white,0,1,(Sport_Recording.MAX_Speed_In_This_Trap*62/100));
					Dis_Play_L16Data_H24(200,19,Truckrun_blue,Truckrun_white,0,1,(u32)(Sport_Recording.AVG_Speed_In_This_Trap*62/100));
					Show_Word_Agency(257,63,0,"mph",Truckrun_blue,Truckrun_white);
				}
				else
				{
					Dis_Play_L16Data_H24(200,102,Truckrun_blue,Truckrun_white,0,1,Sport_Recording.MAX_Speed_In_This_Trap);
					Dis_Play_L16Data_H24(200,19,Truckrun_blue,Truckrun_white,0,1,(u32)(Sport_Recording.AVG_Speed_In_This_Trap));
					Show_Word_Agency(257,63,0,"km/h",Truckrun_blue,Truckrun_white);
				}
			break;
			case 4:
	            Clr_TFTLCD_Area(98,83,162,104,Truckrun_white);
	            Clr_TFTLCD_Area(98,46,162,67,Truckrun_white);
	            Clr_TFTLCD_Area(257,64,320,88,Truckrun_white);
	            Clr_TFTLCD_Area(200,102,315,134,Truckrun_white);
	            Clr_TFTLCD_Area(200,19,315,51,Truckrun_white);
                Dis_Graphblock_bicolor(98,104,64,21,Truckrun_white,Truckrun_blue,word_time,168);
				Dis_Play_L16Data_H24(200,102,Truckrun_blue,Truckrun_white,0,0,Time_For_BF_ShutWithClear);
				Show_Word_Agency(257,63,0,"min",Truckrun_blue,Truckrun_white);
			break;
		}
		Work_Status.Interface_ReLoad_Mask=0;
	}
	if(LCD_DisPlay.WALK_ReLoad_Flag)
	{
		LCD_DisPlay.WALK_ReLoad_Flag=0;
		if(Work_Status.P_Walk_Flag)
		{
			PAS_Scroll_Step = 5;
			PAS_Value_Scroll_Display();
			PAS_Scroll_Step = 0;
			Dis_Play_Walk(1);
		}
		else
		{
			PAS_Scroll_Step = 5;
			PAS_Value_Scroll_Display();
			PAS_Scroll_Step = 4;
		}
	}
	
	if(LCD_DisPlay.PAS_ReLoad_Flag)
	{
		LCD_DisPlay.PAS_ReLoad_Flag = 0;
		if(Work_Status.P_Walk_Flag)
			PAS_Scroll_Step = 0;
		else;
			PAS_Value_Scroll_Display();
	}
	if(PAS_Scroll_Step==0)
	{
		if(LCD_DisPlay.Bat_ReLoad_Flag)//显示电量
		{
			LCD_DisPlay.Bat_ReLoad_Flag=0;
			if(Setting.SOC_Rev_Flag)
				Dis_Power_Left_Continue(Battery_SOC);
			else
			{
				for(v_i=0;v_i<5;v_i++)
				{
					if(SYS_Voltage_Start<SYS_V_Stand_Value[v_i])
						break;
				}
				if(v_i==4)
				{
					if(SYS_Voltage_Start>SYS_V_Stand_Value[v_i])
						v_i++;
				}
				Battery_SOC = v_i*20;
				Dis_Power_Left_Continue(Battery_SOC);
			}
			//Dis_Valtage_Nums(SYS_P_V_Seclect,Battery_SOC,SYS_Voltage_Start);
		}
		if(LCD_DisPlay.Spd_ReLoad_Flag)//显示速度
		{
			LCD_DisPlay.Spd_ReLoad_Flag=0;
//			CCS_Speed=Speed_Smooth_BM(D_Speed);
//			if(CCS_Speed_Stro != CCS_Speed)
//			{
//				CCS_Speed_Stro = CCS_Speed;
				Dis_Play_Speeds(D_Speed);
//			}
		}
//		if(LCD_DisPlay.PAS_ReLoad_Flag)//显示档位值 及 运动模式
//		{
//			LCD_DisPlay.PAS_ReLoad_Flag=0;
//			Dis_Play_ASS(PAS_Value,1);
//		}
		if(LCD_DisPlay.PCC_ReLoad_Flag)//显示功率
		{
			LCD_DisPlay.PCC_ReLoad_Flag=0;
			//Dis_Power_Value(Motor_Power_CC);
		}
		if(LCD_DisPlay.Light_ReLoad_Flag)//显示大灯符号
		{
			LCD_DisPlay.Light_ReLoad_Flag = 0;
			Show_DD_Syb(Work_Status.Light_Switch_Flag);
		}
		if(LCD_DisPlay.ERR_ReLoad_Flag)//故障代码显示
		{
			LCD_DisPlay.ERR_ReLoad_Flag=0;
			if(((Work_Status.Current_Err_CODE>0x20)&&(Work_Status.Current_Err_CODE<0x26))||(Work_Status.Current_Err_CODE==0x30))
			{
					Work_Status.SYS_Err_Occurred_Flag=1;
					DisPlay_Err_Message(Work_Status.Current_Err_CODE);
			}
			else
			{
				if(Work_Status.SYS_Err_Occurred_Flag)
				{
					Work_Status.SYS_Err_Occurred_Flag=0;
					Work_Status.Current_Err_CODE_Bk = 0;
					DisPlay_Err_Message(0);
					Work_Status.Interface_ReLoad_Mask=1;
					Clr_TFTLCD_Area(0,440,215,479,Truckrun_blue);
					Clr_TFTLCD_Area(270,440,319,479,Truckrun_blue);
				}
			}
		}
		if(LCD_DisPlay.USB_ReLoad_Flag)
		{
			LCD_DisPlay.USB_ReLoad_Flag = 0;
			DiaPlay_USB(Work_Status.USB_State_Flag);
		}
	}
}

/*USART-SEND =====>> 串口发送数据轮询*/
void DisPlay_Send_Running_Data(void)
{
	u16 Temp00;
	if((MY_USART.Send_States_Flag==0)||((Setting.In_Flag!=0)&&(PW_Send_Flag == 0)))//????????
    {
          /*****----DATA0----*****/
          Send_Data[0]=0;
          if(SYS_Pus_FR)//???????
              Send_Data[0] |= 0X40;
          Send_Data[0] += SYS_Pus_SCN;//???
          /*****----DATA1----*****/
          Send_Data[1]=0;
          if(SYS_Pus_CPn<10)
              Send_Data[1]=768/PAS_CP_CGN;//????????
          else
          {
              if(SYS_Pus_CPn==10)
                  Send_Data[1]=768/12;
              else
                  Send_Data[1]=768/24;
          }/**/
          /*****----DATA2----*****/
          Send_Data[2]=0;
          if(SYS_HAND_F_Flag)
              Send_Data[2] |= 0x80;
          if(SYS_HAND_L_Flag)
              Send_Data[2] |= 0x40;
          Send_Data[2] |= ((SYS_PUS_SSP<<4)&0x30);//**************????????????????????????????????????????
          //if(SYS_Pus_CPn<10)
          Send_Data[2] += SYS_Pus_CPn;
          /*else
          {
              if(SYS_Pus_CPn==10)
                  Send_Data[2] += 12;
              else
                  Send_Data[2] += 24;
          }*/
          /*****----DATA3----*****/
          Send_Data[3] = 0;
					if(V_Sys_Worked == 3)
					{
						if(SYS_V_Stand_Value[0]>420)
							Send_Data[3] = SYS_V_Stand_Value[0]-420;
						else
						{
							Send_Data[3] = 420-SYS_V_Stand_Value[0];
							Send_Data[3] |= 0x80;
						}
					}
					else if(V_Sys_Worked == 2)
					{
						if(SYS_V_Stand_Value[0]>315)
							Send_Data[3] = SYS_V_Stand_Value[0]-310;
						else
						{
							Send_Data[3] = 315-SYS_V_Stand_Value[0];
							Send_Data[3] |= 0x80;
						}
					}
					else if(V_Sys_Worked == 1)
					{
						if(SYS_V_Stand_Value[0]>210)
							Send_Data[3] = SYS_V_Stand_Value[0]-210;
						else
						{
							Send_Data[3] = 210-SYS_V_Stand_Value[0];
							Send_Data[3] |= 0x80;
						}
					}
          /*****----DATA4----*****/
          Send_Data[4] = (SYS_CUR_Limit_Value*2);
          Send_Data[4] |= (V_Sys_Worked<<6);
          //Send_Data[4] |= 0x80;
          /*****----DATA5----*****/
          Send_Data[5] = 50;
          /*****----DATA6----*****/
					Send_Data[6] = 0;
          Send_Data[6] |= (Speed_Limte_Code)<<3;
          Send_Data[6] |= Wheel_Dia_Mark;//轮径
					Send_Fream_Data_5S(Send_Data,7,'S');
      }
      else
      {
          /*****----DATA0----*****/
		  if((PAS_Value>0) && (Setting.In_Flag==0))
		  {
				Temp00=(u16)((*(PAS_Tab_Set+PAS_Value-1))*255)/100;
			    if(Speed_Over_Limte_Flag)
				{
					if(PAS_Value_Dec<Temp00)
						PAS_Value_Dec++;
				}
				Temp00-=PAS_Value_Dec;
		  }
		  else
				Temp00=0;
          if(Work_Status.P_Walk_Flag)
              Send_Data[0]=Walk_PWM_Temp;
          else
              Send_Data[0]=(u8)Temp00;
		  //(255/(Work_Status.SCA_Leve_Setting/2*2+3))*PAS_Value;
          /*****----DATA1----*****/
          Send_Data[1] = 0;
          if(Work_Status.Light_Switch_Flag)
              Send_Data[1] |= 0x80;
          if(Work_Status.P_Walk_Flag)
				Send_Data[1] |= 0x10;
		  else
				Walk_PWM_Temp=40;
		  if(LOW_Voltage_Flag)
			  Send_Data[1] |= 0x20;
      if(Work_Status.Current_Speed>Real_Speed_Limte_Value)
		  {
        Send_Data[1] |= 0x01;
			  if(++Speed_Over_Limte_Check>2)//?? ???? 1S
			  {
				  Speed_Over_Limte_Check=0;
				  Speed_Over_Limte_Flag=1;
			  }
		  }
		  else
		  {
			  Speed_Over_Limte_Flag=0;
			  Speed_Over_Limte_Check=0;
			  if((((Speed_Limte_Code)*10)-Work_Status.Current_Speed)>20)//?????????2km/h
				  PAS_Value_Dec=0;
		  }
		  if(++Walk_Add_Delay>5)
		  {
			  Walk_Add_Delay=0;
			  if(Work_Status.Current_Speed<58)
			  {
					if(Walk_PWM_Temp<80)
						Walk_PWM_Temp+=2;
			  }
			  else
			  {
				  if(Work_Status.Current_Speed>60)
				  {
						if(Walk_PWM_Temp>40)
							Walk_PWM_Temp-=2;
				  }
			  }
		  }
		  Send_Fream_Data_5S(Send_Data,2,'R');
      }
}

/*USART-REV ---->  串口接收数据处理*/
void J_DisPlay_Receive_CallBack(void)
{
	uint8_t  PC_Commond , i=0 , j=0 , vcl[5];
	uint8_t  Sum_Check_M , Sum_Check0;
	uint32_t V_Temp6 , C_Temp10[20];
	uint16_t M_Speed;
	Work_Status.NoCommunication_OverFlow=0;
	MY_USART.REV_Back_OUT_Time = 0;
	if(Work_Status.Current_Err_CODE == 0x30)
	{
		Work_Status.Current_Err_CODE=0;
		Work_Status.SYS_Err_Occurred_Flag=0;
		Work_Status.Interface_ReLoad_Mask=1;
		Clr_TFTLCD_Area(0,440,319,479,Truckrun_blue);
	}
	if((Rx_Buf[0]==0x3a) && (Rx_Buf[1]==0x1c) && (Rx_Buf[RX_NUM-1]==0x0a) && (Rx_Buf[RX_NUM-2]==0x0d))//判断是否PC端配置软件发来的数据
	{
		RX_NUM-=4;
		Sum_Check0=Rx_Buf[RX_NUM];
		RX_NUM--;
		Sum_Check_M=0;
		while(RX_NUM>0)//数据校验
		{
			Sum_Check_M+=Rx_Buf[RX_NUM];
			RX_NUM--;
		}
		if(Sum_Check_M==Sum_Check0)//校验和检验
		{
			Work_Status.Call_IN_From_PC_Flag=1;
			PC_Commond=Rx_Buf[2];
		}
	}
	else
	{
		Work_Status.Call_IN_From_PC_Flag=0;
//		Calibration_V_Standard=0;
	}
    RX_NUM=0;
  if(Work_Status.Call_IN_From_PC_Flag==0)//正常通信
	{
		
//		if(Work_Status.Current_Err_CODE==0x30)
//			Work_Status.Current_Err_CODE=0;
		if(MY_USART.Send_States_Flag)
		{
			if(Rx_Buf[2]=='R')//系统运行数据
			{
				C_Current=Rx_Buf[5];
				Motor_Power_CC=(u16)(((SYS_Voltage/10)*C_Current)/3);
				if(Motor_Power_CC!=Motor_Power_CC_Br)//显示功率
				{
					Motor_Power_CC_Br=Motor_Power_CC;
					LCD_DisPlay.PCC_ReLoad_Flag=1;
				}
//				Wheel_T=(unsigned int)((Rx_Buf[6]<<8)+Rx_Buf[7]);//显示速度
				Wheel_T=Rx_Buf[6];
				Wheel_T*=256;
				Wheel_T+=Rx_Buf[7];
				if(Wheel_T<3000)
				{
					if(S_Wheel_T < Wheel_T)
					{
						if((2*S_Wheel_T)>Wheel_T)
						{
							if(((2*S_Wheel_T)-Wheel_T)<6)
								SPD_ERR=1;
							else
								SPD_ERR=0;
						}
						else
						{
							SPD_ERR=1;
						}	
					}
					else
						SPD_ERR=0;
				}
				else
					SPD_ERR=0;
				S_Wheel_T = Wheel_T;
				if(SPD_ERR == 0)//???????
				{
					Mileage_Variable.Base_Time_Count_Delay=Wheel_T;//????????
					if(Wheel_T>3490)//???0
					{
						Work_Status.Bike_Moving_Flag=0;
						Work_Status.Current_Speed=0;
						//Get_Speed_AVGSD(0);
					}
					else
					{
						Work_Status.Bike_Moving_Flag=1;
						Work_Status.Current_Speed=(Speed_Base_Value/Wheel_T);
						if(Work_Status.Speed_Unit_GY_Flag)//????????????
						{
							M_Speed = (Work_Status.Current_Speed * 100)/62;
							if((M_Speed*62/100)<Work_Status.Current_Speed)
								M_Speed+=1;
						}
						else
							M_Speed = Work_Status.Current_Speed;
						if(Sport_Recording.MAX_Speed_In_This_Trap<M_Speed)//??????
						{
							Sport_Recording.MAX_Speed_In_This_Trap=M_Speed;
							Work_Status.MAX_SPD_Reset=1;
							if(Work_Status.Working_Mode == 2)
								Work_Status.Interface_ReLoad_Mask = 1;
						}
					}
					#if LOCK_WALK_Speed_DIS
					if(Work_Status.P_Walk_Flag)
						D_Speed=WALK_Speed_Def;
					else
					#endif
						D_Speed=Work_Status.Current_Speed;//实时速度
					if(D_Speed_Stro != D_Speed)
					{
						D_Speed_Stro = D_Speed;
						LCD_DisPlay.Spd_ReLoad_Flag = 1;
					}
				}
				Work_Status.Current_Err_CODE=Rx_Buf[8];//判断错误标识
				if((Work_Status.Current_Err_CODE == 0x21) ||(Work_Status.Current_Err_CODE == 0x22) ||(Work_Status.Current_Err_CODE == 0x23) ||(Work_Status.Current_Err_CODE == 0x24) ||(Work_Status.Current_Err_CODE == 0x25))
				{
					if(Work_Status.Current_Err_CODE_Bk != Work_Status.Current_Err_CODE)
					{
						Work_Status.Current_Err_CODE_Bk = Work_Status.Current_Err_CODE;
						Work_Status.SYS_Err_Occurred_Flag = 1;
						LCD_DisPlay.ERR_ReLoad_Flag = 1;
					}
				}
				else
				{
					if(Work_Status.SYS_Err_Occurred_Flag)
					{
						LCD_DisPlay.ERR_ReLoad_Flag = 1;
					}
				}
			}
		}
		else
		{
			if(Rx_Buf[2]=='S')
				MY_USART.Send_States_Flag=1;
		}
	}
	else//--------处理PC指令----------------
	{
		if(PC_Commond==0x5d)//电压校准指令
		{
			ADC_Value[147] = 0;
			Clr_TFTLCD_Area(0,200,319,280,White1);
			Show_Word_Agency(10,250,0,(u8 *)"Voltage Calibration..." ,Red , White1);
			delay_ms(5);
			while(ADC_Value[147] == 0)
			;
			V_Temp6 = 0;
			for(i=0;i<50;i++)
				V_Temp6 += ADC_Value[3*i];
			V_Standard_Value = V_Temp6/360;
			Read_Save_VStandard(0);
			vcl[0]=V_Standard_Value/100%10 + '0';
			vcl[1]=V_Standard_Value/10%10 + '0';
			vcl[2]=V_Standard_Value%10 + '0';
			vcl[3]=0;
			Show_Word_Agency(10,210,0,(u8 *)"Reference:" ,Red , White1);
			Show_Word_Agency(190,210,0,vcl , Green , White1);
			delay_ms(500);
			Int_Index_DisPlay();
		}
		else if(PC_Commond==0x6E)
		{
			//以下校准光感流程
			Clr_TFTLCD_Area(0,200,319,280,White1);
			Show_Word_Agency(10,250,0,(u8 *)"AL Calibration..." ,Red , White1);
			for(j=0;j<20;j++)
			{
					ADC_Value[149] = 0;
					delay_ms(10);
					while(ADC_Value[149] == 0)
					;
					V_Temp6 = 0;
					for(i=0;i<50;i++)
						V_Temp6 += ADC_Value[2 + (3*i)];
					C_Temp10[j] = V_Temp6/50;
			}
			V_Temp6 = 0;
			for(j=0;j<20;j++)
			{
					V_Temp6 += C_Temp10[j];
			}
			V_Temp6/=20;
			if(V_Temp6>3000)
			{
					AL_Standard_Value = V_Temp6;
					Read_Save_AL_Standard(0);
					vcl[0]=AL_Standard_Value/1000%10 + '0';
					vcl[1]=AL_Standard_Value/100%10 + '0';
					vcl[2]=AL_Standard_Value/10%10 + '0';
					vcl[3]=AL_Standard_Value%10 + '0';
					vcl[4]=0;
					Show_Word_Agency(10,210,0,(u8 *)"Reference:" ,Red , White1);
					Show_Word_Agency(190,210,0,vcl , Green , White1);
			}
			else
					Show_Word_Agency(10,210,0,(u8 *)"Cover The Sensor!" ,Red , White1);
			delay_ms(800);
			Int_Index_DisPlay();
		}
		else if(PC_Commond==0x71) // 校准时间,时/分/秒   同时校准光感
		{
//			Timer_Hour = Rx_Buf[4];
//			Timer_Minute = Rx_Buf[5];
//			Timer_Second = Rx_Buf[6];
//			RTC_Set_Time(Timer_Hour , Timer_Minute , Timer_Second);
			LCD_DisPlay.TIME_ReLoad_Flag = 1;
			
		}
		else if(PC_Commond==0x8E) // 清零 总里程数据  ODO Clear
		{
//			Show_Word_Agency(260,110,0,(u8 *)"OK", Green , Black);
			Sport_Recording.Total_Trap_Record=0;
			Read_Save_Total_Trap_Record(0);
			delay_ms(5);
			Save_Total_Trap_Bak();
			delay_ms(5);
			Sport_Recording.Single_Trap_At_Once = 0;
			Sport_Recording.Timer_Of_This_Trap = 0;
			Read_Save_Single_Trap(0);
			delay_ms(5);
			Sport_Recording.MAX_Speed_In_This_Trap=0;
			Read_Save_MAX_Trap(0);
			Work_Status.Interface_ReLoad_Mask=1;
//			Show_Word_Agency(260,110,0,(u8 *)"    ", Green , Black);
		}
		Work_Status.Call_IN_From_PC_Flag=0;
	}
}

u8 Comper_Strings_Eque(u8 *A1 , u8 *B1 , u8 Les)
{
	u8 i;
	for(i=0;i<Les;i++)
	{
		if(A1[i]!=B1[i])
			return 0;
	}
	return 1;		
}
/*SETTINGS ---->  设置界面处理*/
void Setting_Menu_List(void)
{
	if(Setting.Setting_ReLoad_Mask)
	{
		Setting.Setting_ReLoad_Mask=0;
		if(Setting.Exit_Flag)
		{
			Setting.Mode=0;
			Setting.In_Flag=0;
			if(Setting.PassWord_Changed_Flag)
			{
				Work_Status.SYS_Start_PassWord_EnAble = Work_Status.SYS_Start_PassWord_SBuf;
				Setting.PassWord_Changed_Flag = 0;
			}
			Config_Temp[5] = 0;
			Work_Status.Speed_Unit_GY_Flag 	= 	Config_Temp[0];
			if(Speed_Limte_BUF != Config_Temp[8])
			{
				if(Work_Status.Speed_Unit_GY_Flag)
						Speed_Limte_Code = SPEED_UNIT_CONVERT(0,Config_Temp[8])-10;
				else
						Speed_Limte_Code = Config_Temp[8] - 10;
			}
			Wheel_Dia_Mark								=   Config_Temp[6];
			SYS_P_V_Seclect								=		Config_Temp[3];
			SYS_CUR_Limit_Value 					= 	Config_Temp[4];
			SYS_Pus_CPn 									= 	Config_Temp[10];
			PAS_CP_CGN 										= 	Config_Temp[11];
			SYS_Auto_OFF 									= 	Config_Temp[2];
			Work_Status.BLK_Light_Level 	= 	Config_Temp[1];
			AUTO_LIGHT_Sensitivity 				= 	Config_Temp[9];
			Work_Status.SCA_Leve_Setting 	= 	Config_Temp[12];
			SYS_PUS_SSP										= 	Config_Temp[13];
			V_Stand_Sel										= 	Config_Temp[7];
			//Set_Valtage_Segment();
			Read_Save_5SEG_Valtage(0);
			if(V_Stand_Sel>3)
				V_Sys_Worked = 3;
			else
				V_Sys_Worked = 2;
			Set_Power_PAS_DW();
			Read_Save_SYS_Setting_Parameter(0);
			if(AUTO_LIGHT_Sensitivity!=0)
			{
				Work_Status.Light_Sensing_EN = 1;
				if(AUTO_LIGHT_Sensitivity==5)
						LIGHT_ON_SET = AL_Standard_Value-160;
				else if(AUTO_LIGHT_Sensitivity==4)
						LIGHT_ON_SET = AL_Standard_Value-120;
				else if(AUTO_LIGHT_Sensitivity==3)
						LIGHT_ON_SET = AL_Standard_Value-80;
				else if(AUTO_LIGHT_Sensitivity==2)
						LIGHT_ON_SET = AL_Standard_Value-60;
				else if(AUTO_LIGHT_Sensitivity==1)
						LIGHT_ON_SET = AL_Standard_Value-40;
				LIGHT_OFF_SET = (LIGHT_ON_SET-20);
			}
			else
			{
				Work_Status.Light_Sensing_EN = 0;
			}
			if(Setting.PAS_Val_Changed_Flag)
			{
				Setting.PAS_Val_Changed_Flag = 0;
				Read_Save_PAS_Value(0);//写助力百分比
			}
		}
		else
		{
			if(Setting.Mode==0)//一级菜单
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
//					Dis_Play_Setting_BAck_Ground(Setting.View_S);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					if(Setting.Chosen == 0)
					{
						Setting.Card_ReLoad_Flag=0;
						Setting_Value_Bottom_Limit=0;
						Setting_Value_Temp=6-Setting.Selected;
						Setting_Value_Top_Limit=5;
					}
					else
					{
						if(Setting.View_S==0)
						{
							switch(Setting.Chosen)
							{
								case 1:
									Setting_Value_Temp=Config_Temp[Setting.Chosen - 1];
									Setting_Value_Top_Limit=1;
									Setting_Value_Bottom_Limit=0;
								break;
								case 4:
									Setting_Value_Temp=Config_Temp[5];
									Setting_Value_Top_Limit=1;
									Setting_Value_Bottom_Limit=0;
								break;
								case 2:
									Setting_Value_Temp=Config_Temp[1];
									Setting_Value_Top_Limit=5;
									Setting_Value_Bottom_Limit=1;
								break;
								case 3:
									Setting_Value_Temp=Config_Temp[2];
									Setting_Value_Top_Limit=9;
									Setting_Value_Bottom_Limit=0;
								break;
							}
						}
						else
						{
							switch(Setting.Chosen)
							{
								case 1://轮径参数
									Setting_Value_Temp=Config_Temp[6];
									Setting_Value_Top_Limit=7;
									Setting_Value_Bottom_Limit=0;
								break;
								case 2://限速参数
									Setting_Value_Temp=Config_Temp[8];
									if(Config_Temp[0])
										Setting_Value_Top_Limit=40;
									else
										Setting_Value_Top_Limit=60;
									Setting_Value_Bottom_Limit=10;
								break;
								case 3://限流参数
									Setting_Value_Temp=Config_Temp[4];
									Setting_Value_Top_Limit=30;
									Setting_Value_Bottom_Limit=10;
								break;
								case 4://速度传感器磁钢数
									Setting_Value_Temp=Config_Temp[10];
									Setting_Value_Top_Limit=12;
									Setting_Value_Bottom_Limit=1;
								break;
								case 5://系统电压设置
									Setting_Value_Temp=Config_Temp[7];
									Setting_Value_Top_Limit=2;
									Setting_Value_Bottom_Limit=0;
								break;
							}
						}
					}
				}
				if(Setting.Clrear_Trip_Flag)
				{
					Setting.Clrear_Trip_Flag = 0;
					Sport_Recording.Single_Trap_At_Once=0;
					Sport_Recording.Timer_Of_This_Trap=0;
					Read_Save_Single_Trap(0);
					Sport_Recording.MAX_Speed_In_This_Trap = 0;
					Read_Save_MAX_Trap(0);
					Sport_Recording.AVG_Speed_In_This_Trap = 0;
					Config_Temp[5] = 2;
				}
				if(Setting.Chosen == 0)
						Setting.Selected=6-Setting_Value_Temp;
				else
				{
					if(Setting.View_S==0)
					{
						if(Setting.Chosen<4)
							Config_Temp[Setting.Chosen - 1]=Setting_Value_Temp;
						else if(Setting.Chosen == 4)//TRIP CLEAR
							Config_Temp[5]=Setting_Value_Temp;
						if(Setting.Chosen == 2)
							Set_BK_Light_Leve(Config_Temp[1]);
					}
					else
					{
						if(Setting.Chosen==1)//轮径
							Config_Temp[6]=Setting_Value_Temp;
						else if(Setting.Chosen==2)//限速
							Config_Temp[8]=Setting_Value_Temp;
						else if(Setting.Chosen==3)//限流
							Config_Temp[4]=Setting_Value_Temp;
						else if(Setting.Chosen==4)//速度传感器磁钢数
							Config_Temp[10]=Setting_Value_Temp;
						else if(Setting.Chosen==5)//系统电压
							Config_Temp[7]=Setting_Value_Temp;
					}
				}
				Dis_Setting_View(Setting.View_S , (Setting.Selected + (Setting.Chosen<<4)) , Config_Temp);
			}
			else if(Setting.Mode==1)//二级菜单  密码界面
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
					Dis_Play_PassWord_Ground(0);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					if(Setting.Chosen == 0)
					{
						if(Code_Input_Flag==0)
						{
							if(Work_Status.SYS_Start_PassWord_EnAble)
							{
									Setting_Value_Temp=2;
									Setting_Value_Top_Limit=2;
									Setting_Value_Bottom_Limit=0;
							}
							else
							{
									Setting_Value_Temp=1;
									Setting_Value_Top_Limit=1;
									Setting_Value_Bottom_Limit=0;
							}
						}
						else
						{
							Setting_Value_Temp=User_Input_Code_Tab[Code_Input_Flag-1];
							Setting_Value_Top_Limit=9;
							Setting_Value_Bottom_Limit=0;
						}
					}
					else
					{
						Setting_Value_Temp=Work_Status.SYS_Start_PassWord_EnAble;
						Setting_Value_Top_Limit=1;
						Setting_Value_Bottom_Limit=0;
					}
				}
				if(Setting.Chosen>0)
					Work_Status.SYS_Start_PassWord_EnAble = Setting_Value_Temp;
				else
				{
					if(Code_Input_Flag==0)
					{
						if(Work_Status.SYS_Start_PassWord_EnAble)
								Setting.Selected = 3-Setting_Value_Temp;
						else
								Setting.Selected = 2-Setting_Value_Temp;
					}
					else
						User_Input_Code_Tab[Code_Input_Flag-1] = Setting_Value_Temp;
				}
				if(Setting.PW_Set_MSG_Flag>0)
				{
					DisPlay_Full_Messages(Setting.PW_Set_MSG_Flag);
					if(Setting.Save_PassWord_Flag)
					{
						Setting.Save_PassWord_Flag = 0;
						Setting.PassWord_Changed_Flag = 0;
						Read_Save_SYS_PassWord(0);
					}
				}
				else
				{
					if(Setting.Password_Set_States)
							DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , Setting.Password_Set_States);
					else
							Dis_PassWord_Setting_View(Setting.Selected + (Setting.Chosen<<4) , Work_Status.SYS_Start_PassWord_EnAble);
				}
			}
			else if(Setting.Mode==3)//四级菜单  电池信息界面
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag = 0;
					Voltage_5Seg_Ground();
					V_Stand_Sel = Config_Temp[7];
					if(V_Stand_Sel_SBuf != V_Stand_Sel)
					{
						V_Stand_Sel_SBuf = V_Stand_Sel;
						Set_Valtage_Segment();
					}
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					if(V_Setting_Sel==1)
					{
						Setting_Value_Temp=SYS_V_Stand_Value[0];
						Setting_Value_Top_Limit=SYS_V_Stand_Value[1];
						Setting_Value_Bottom_Limit=SYS_V_Stand_Value[0]-10;
					}
					else if(V_Setting_Sel==5)
					{
						Setting_Value_Temp=SYS_V_Stand_Value[4];
						Setting_Value_Top_Limit=SYS_V_Stand_Value[4]+10;
						Setting_Value_Bottom_Limit=SYS_V_Stand_Value[3];
					}
					else
					{
						Setting_Value_Temp=SYS_V_Stand_Value[V_Setting_Sel-1];
						Setting_Value_Top_Limit=SYS_V_Stand_Value[V_Setting_Sel];
						Setting_Value_Bottom_Limit=SYS_V_Stand_Value[V_Setting_Sel-2];
					}
				}
				SYS_V_Stand_Value[V_Setting_Sel-1] = Setting_Value_Temp;
				Voltage_5Seg_Setting(V_Setting_Sel , SYS_V_Stand_Value);
			}
			else if(Setting.Mode==4)//五级菜单  助力百分比设置
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag = 0;
					Work_Status.SCA_Leve_Setting 	= 	Config_Temp[12];
					Set_Power_PAS_DW();
//					PAS_PreValue_Ground(PAS_Top_Vf);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					if(P_Setting_Selc==1)
					{
						Setting_Value_Temp=*PAS_Tab_Set;
						Setting_Value_Top_Limit=*(PAS_Tab_Set+1);
						Setting_Value_Bottom_Limit=1;
					}
					else if(P_Setting_Selc==PAS_Top_Vf)
					{
						Setting_Value_Temp=*(PAS_Tab_Set+PAS_Top_Vf-1);
						Setting_Value_Top_Limit=100;
						Setting_Value_Bottom_Limit=*(PAS_Tab_Set+PAS_Top_Vf-2);
					}
					else
					{
						Setting_Value_Temp=*(PAS_Tab_Set+P_Setting_Selc-1);
						Setting_Value_Top_Limit=*(PAS_Tab_Set+P_Setting_Selc);
						Setting_Value_Bottom_Limit=*(PAS_Tab_Set+P_Setting_Selc-2);
					}
				}
//				*(PAS_Tab_Set+P_Setting_Selc-1) = Setting_Value_Temp;
//				PAS_PreValue_Setting(PAS_Top_Vf , P_Setting_Selc , PAS_Tab_Set);
			}
			else if(Setting.Mode==MY_SETTING_INDEX)//设置页面首页 index
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					Setting_Value_Temp=0;
					Setting_Value_Top_Limit=1;
					Setting_Value_Bottom_Limit=0;
				}
				Setting.Selected = Setting_Value_Temp;
				SETTING_INDEX_Page(Setting_Value_Temp);
				Dis_Setting_View(Setting_Value_Temp,0,Config_Temp);
//				SETTING_INDEX_Page(Setting.Selected);
			}
			if(Setting.Mode==Advanced_Pas_Ent)//高级设置输入密码进入
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
					Dis_Play_PassWord_Ground(0);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					Setting.Password_Set_States = 15;
					Setting_Value_Temp=User_Input_Code_Tab[Code_Input_Flag-1];
					Setting_Value_Top_Limit=9;
					Setting_Value_Bottom_Limit=0;
				}
				User_Input_Code_Tab[Code_Input_Flag-1] = Setting_Value_Temp;
				DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , Setting.Password_Set_States);
			}
		}
	}
}
/* KEY ---->  >>按键处理程序*/
void Key_Mission_Process(u8 Key_Value9)
{
	u16 i;
	switch(Key_Value9)
	{
		case KEY_VALUE_i_S://短按 i  单击
				if(Setting.In_Flag==0)//非设置模式。切换档位
				{
					Work_Status.Interface_ReLoad_Mask=1;
					if(Work_Status.Working_Mode<4)
						Work_Status.Working_Mode++;
					else
						Work_Status.Working_Mode=0;
				}
				else
				{
					if(Setting.Mode==0)//一级菜单，选定/取消选定项目
					{
						if(Setting.View_S == 0)//display setting
						{
							if(Setting.Selected==6)//返回主菜单
							{
								Setting.Setting_ReLoad_Mask = 1;
								Setting.Exit_Flag=1;
							}
							else if(Setting.Selected==5)//跳转至密码设置页面
							{
								Setting.Mode = 1;
								Setting.UpDat_Flag = 1;
								Setting.Card_ReLoad_Flag = 1;
								Work_Status.SYS_Start_PassWord_SBuf = Work_Status.SYS_Start_PassWord_EnAble;
								Setting.PassWord_Changed_Flag = 1;
								Setting.Password_Set_States = 0;
								Setting.PW_Set_MSG_Flag = 0;
								Setting.PW_Set_MSG_Delay = 0;
								Setting.Chosen = 0;
								Code_Input_Flag = 0;
								User_Input_Code_Tab[0]=10;
								User_Input_Code_Tab[1]=10;
								User_Input_Code_Tab[2]=10;
								User_Input_Code_Tab[3]=10;
							}
							else//进入深度设置模式
							{
								if(Setting.Chosen == 0)//选定项目
								{
									Setting.Chosen = Setting.Selected;
									if(Setting.Selected == 4)
										Config_Temp[5] = 0;
								}
								else
								{
									if(Setting.Selected == 4)
									{
										if(Config_Temp[5] == 1)//清零单次里程
											Setting.Clrear_Trip_Flag = 1;
									}/**/
									Setting.Chosen = 0;
								}
								Setting.Card_ReLoad_Flag=1;
							}
						}
						else if(Setting.View_S == 1)
						{
							if(Setting.Selected==6)//返回主菜单
							{
								Setting.Setting_ReLoad_Mask = 1;
								Setting.Exit_Flag=1;
							}
							else
							{
								if(Setting.Chosen == 0)//选定项目
									Setting.Chosen = Setting.Selected;
								else
									Setting.Chosen = 0;
								Setting.Card_ReLoad_Flag=1;
							}
						}
					}
					else if(Setting.Mode==1)
					{
						if(Setting.Selected == 1)
						{
							Setting.Setting_ReLoad_Mask = 1;
							Setting.Exit_Flag=1;
						}
						else if(Setting.Selected == 2)
						{
							if(Setting.Chosen == 0)
							{
								Setting.Chosen = Setting.Selected;
								Setting.PW_Last_States = Work_Status.SYS_Start_PassWord_EnAble;
							}
							else
							{
								Setting.Chosen = 0;
								if((Setting.PW_Last_States == 0) && (Work_Status.SYS_Start_PassWord_EnAble == 1))
								{
									Setting.Password_Set_States=2;
									Setting.Selected = 4;
									Code_Input_Flag = 1;
									Setting.UpDat_Flag = 1;
									for(i=0;i<4;i++)
											User_Input_Code_Tab[i]=10;
								}
								else if((Setting.PW_Last_States == 1) && (Work_Status.SYS_Start_PassWord_EnAble == 0))
								{
									Setting.Password_Set_States=6;
									Setting.Selected = 4;
									Code_Input_Flag = 1;
									Setting.UpDat_Flag = 1;
									for(i=0;i<4;i++)
											User_Input_Code_Tab[i]=10;
								}
							}
						}
						else if(Setting.Selected == 3)
						{
							Setting.Selected = 4;
							Code_Input_Flag = 1;
							Setting.Chosen = 0;
							Setting.Password_Set_States = 9;
							Setting.UpDat_Flag = 1;
							for(i=0;i<4;i++)
									User_Input_Code_Tab[i]=10;
						}
						else if(Setting.Selected == 4)//密码输入  模式
						{
							if(Code_Input_Flag<4)
							{
								if(User_Input_Code_Tab[Code_Input_Flag-1]<10)
									Code_Input_Flag++;
							}
							else if(User_Input_Code_Tab[Code_Input_Flag-1]<10)
							{
								if(Setting.Start_Code_Flag)
									Code_Input_Flag=5;
								else
								{
									switch(Setting.Password_Set_States)
									{
											case 2:
												Setting.Password_Set_States = 3;
												for(i=0;i<4;i++)
													User_Input_Code_Buf[i]=User_Input_Code_Tab[i];
												for(i=0;i<4;i++)
													User_Input_Code_Tab[i]=10;
												Code_Input_Flag = 1;
											break;
											case 3:
												if(Comper_Strings_Eque(User_Input_Code_Buf,User_Input_Code_Tab,4))
//												if((User_Input_Code_Buf[0]==User_Input_Code_Tab[0])&&(User_Input_Code_Buf[1]==User_Input_Code_Tab[1])&&(User_Input_Code_Buf[2]==User_Input_Code_Tab[2])&&(User_Input_Code_Buf[3]==User_Input_Code_Tab[3]))
												{
													Setting.PW_Set_MSG_Flag = 1;//提示设置成功
													Setting.Selected = 1;
													Code_Input_Flag = 0;
													Setting.Password_Set_States=0;
													Setting.Save_PassWord_Flag = 1;
													for(i=0;i<4;i++)
														Sys_Code_Tab[i]=User_Input_Code_Tab[i];
												}
												else
												{
													Setting.Password_Set_States = 3;
													Setting.PW_Set_MSG_Flag = 2;//提示确认失败
													for(i=0;i<4;i++)
														User_Input_Code_Tab[i]=10;
													Code_Input_Flag = 1;
												}
											break;
											case 6:
												if(Comper_Strings_Eque(Sys_Code_Tab,User_Input_Code_Tab,4))
//												if((Sys_Code_Tab[0]==User_Input_Code_Tab[0])&&(Sys_Code_Tab[1]==User_Input_Code_Tab[1])&&(Sys_Code_Tab[2]==User_Input_Code_Tab[2])&&(Sys_Code_Tab[3]==User_Input_Code_Tab[3]))
												{
													Setting.PW_Set_MSG_Flag = 3;//提示设置成功
													Setting.Selected = 1;
													Code_Input_Flag = 0;
													Setting.Password_Set_States=0;
													Setting.Save_PassWord_Flag = 1;
												}
												else
												{
													Setting.PW_Set_MSG_Flag = 4;//提示失败
													Setting.Password_Set_States = 6;
													for(i=0;i<4;i++)
														User_Input_Code_Tab[i]=10;
													Code_Input_Flag = 1;
												}
											break;
											case 9:
												if(Comper_Strings_Eque(Sys_Code_Tab,User_Input_Code_Tab,4))
//												if((Sys_Code_Tab[0]==User_Input_Code_Tab[0])&&(Sys_Code_Tab[1]==User_Input_Code_Tab[1])&&(Sys_Code_Tab[2]==User_Input_Code_Tab[2])&&(Sys_Code_Tab[3]==User_Input_Code_Tab[3]))
												{
													Setting.Password_Set_States=10;
												}
												else
												{
													Setting.PW_Set_MSG_Flag = 4;//提示失败
													Setting.Password_Set_States = 9;
												}
												for(i=0;i<4;i++)
													User_Input_Code_Tab[i]=10;
												Code_Input_Flag = 1;
											break;
											case 10:
												Setting.Password_Set_States = 11;
												for(i=0;i<4;i++)
													User_Input_Code_Buf[i]=User_Input_Code_Tab[i];
												for(i=0;i<4;i++)
													User_Input_Code_Tab[i]=10;
												Code_Input_Flag = 1;
											break;
											case 11:
												if(Comper_Strings_Eque(User_Input_Code_Buf,User_Input_Code_Tab,4))
//												if((User_Input_Code_Buf[0]==User_Input_Code_Tab[0])&&(User_Input_Code_Buf[1]==User_Input_Code_Tab[1])&&(User_Input_Code_Buf[2]==User_Input_Code_Tab[2])&&(User_Input_Code_Buf[3]==User_Input_Code_Tab[3]))
												{
													Setting.PW_Set_MSG_Flag = 5;//提示设置成功
													Setting.Selected = 1;
													Code_Input_Flag = 0;
													Setting.Password_Set_States=0;
													Setting.Save_PassWord_Flag = 1;
													for(i=0;i<4;i++)
														Sys_Code_Tab[i]=User_Input_Code_Tab[i];
												}
												else
												{
													Setting.Password_Set_States = 11;
													Setting.PW_Set_MSG_Flag = 2;//提示确认失败
													for(i=0;i<4;i++)
														User_Input_Code_Tab[i]=10;
													Code_Input_Flag = 1;
												}
											break;
									}
								}
							}
						}
						Setting.Card_ReLoad_Flag=1;
					}
					else if(Setting.Mode==3)//电压设置界面
					{
						if(V_Setting_Sel<5)
						{
							V_Setting_Sel++;
						}
						else
						{
							Setting.Mode=0;
							V_Setting_Sel=0;
							Setting.Selected=6;
							Setting.UpDat_Flag = 1;
						}
						Setting.Card_ReLoad_Flag = 1;
					}
					else if(Setting.Mode==4)
					{
						if(P_Setting_Selc<PAS_Top_Vf)
							P_Setting_Selc++;
						else
						{
							P_Setting_Selc = 0;
							Setting.Mode=0;
							Setting.Selected=7;
							Setting.UpDat_Flag = 1;
						}
						Setting.Card_ReLoad_Flag = 1;
					}
					else if(Setting.Mode==MY_SETTING_INDEX)
					{
						Setting.View_S = Setting.Selected;
						Setting.Mode = 0;
						Setting.UpDat_Flag = 1;
						Setting.Card_ReLoad_Flag = 1;
						Setting.Selected = 1;
					}
					else if(Setting.Mode==Advanced_Pas_Ent)//输入密码进入高级设置
					{
						if(Code_Input_Flag<4)
						{
							if(User_Input_Code_Tab[Code_Input_Flag-1]<10)
								Code_Input_Flag++;
						}
						else
						{
							if((Comper_Strings_Eque((u8 *)Factry_PassW,User_Input_Code_Tab,4))||(Comper_Strings_Eque((u8 *)Admin_PassW,User_Input_Code_Tab,4)))
//							if((User_Input_Code_Tab[0]==1)&&(User_Input_Code_Tab[1]==6)&&(User_Input_Code_Tab[2]==6)&&(User_Input_Code_Tab[3]==5))
							{
								Setting.Mode = 0;
								Setting.UpDat_Flag = 1;
								Setting.Card_ReLoad_Flag = 1;
								Setting.View_S = 1;
								if(Setting.Selected == 9)
								{
									if(Config_Temp[0])
									{
										Speed_Limte_MPH = Config_Temp[8];
										Speed_Limte_BUF = SPEED_UNIT_CONVERT(0,Config_Temp[8]);
									}
									else
									{
										Speed_Limte_MPH = SPEED_UNIT_CONVERT(1,Config_Temp[8]);
										Speed_Limte_BUF = Config_Temp[8];
									}
								}
							}
							else
							{
								Code_Input_Flag = 1;
								for(i=0;i<4;i++)
									User_Input_Code_Tab[i]=10;
							}
						}
						Setting.Card_ReLoad_Flag=1;
					}
					Setting.Setting_ReLoad_Mask=1;
					Work_Status.SYS_Setting_OverFlow=0;
				}
		break;
		case KEY_VALUE_UP_L|KEY_VALUE_DOWN_L:
				if(Work_Status.Bike_Moving_Flag==0)
				{
					if(Setting.In_Flag==0)//进入设置模式首页
					{
						Setting.In_Flag=1;
						Setting.Mode=MY_SETTING_INDEX;
						Setting.UpDat_Flag=1;
						Setting.Card_ReLoad_Flag=1;
						Setting.Setting_ReLoad_Mask=1;
						Setting.View_S = 0;
						Setting.Selected = 1;
						Setting.Chosen = 0;
						Bat_Index_Pages = 0;
						Setting.Restor_Now_Flag = 0;
						Setting.Save_PassWord_Flag = 0;
						Speed_Buf = 0;
						V_Setting_Sel = 0;
						P_Setting_Selc = 0;
						Setting.PAS_Val_Changed_Flag = 0;
						V_Stand_Sel_SBuf = V_Stand_Sel;
						Config_Temp[0] = Work_Status.Speed_Unit_GY_Flag;
						Config_Temp[9] = AUTO_LIGHT_Sensitivity;
						Speed_Limte_MPH = SPEED_UNIT_CONVERT(1,Speed_Limte_Code + 10);
						if(Work_Status.Speed_Unit_GY_Flag)
								Config_Temp[8] = Speed_Limte_MPH;
						else
								Config_Temp[8] = Speed_Limte_Code + 10;
						Speed_Limte_BUF = Speed_Limte_Code + 10;
						Config_Temp[10] = SYS_Pus_CPn;
						Config_Temp[11] = PAS_CP_CGN;
						Config_Temp[7] = 	V_Stand_Sel;
						Config_Temp[6] = Wheel_Dia_Mark;
						Config_Temp[4] = SYS_CUR_Limit_Value;
						Config_Temp[3] = SYS_P_V_Seclect;
						Config_Temp[2] = SYS_Auto_OFF;
						Config_Temp[1] = Work_Status.BLK_Light_Level;
						Config_Temp[12] = Work_Status.SCA_Leve_Setting;
						Config_Temp[13] = SYS_PUS_SSP;
					}
					else//退出设置，并保存参数
					{
						if(Work_Status.SYS_Err_Occurred_Flag!=0)
							LCD_DisPlay.ERR_ReLoad_Flag=1;
						Setting.Exit_Flag=1;
						Setting.Setting_ReLoad_Mask = 1;
					}
				}
		break;
		case KEY_VALUE_UP_S://短按 up
			if(Setting.In_Flag==0)
			{
					if(PAS_Value<PAS_Top_Vf)//(Work_Status.SCA_Leve_Setting/2*2+3))
					{
						PAS_Value++;
						if(PAS_Scroll_Step==0)
							PAS_Scroll_Step = 5;
						else
							PAS_Scroll_Step = 6;
//						LCD_DisPlay.PAS_ReLoad_Flag=1;
					}
					else
					{
						PAS_Scroll_Step = 5;
//						PAS_Value_NG=1;
					}
			}
			else
			{
				if(Setting_Value_Temp<Setting_Value_Top_Limit)
            Setting_Value_Temp++;
        else
        {
            if(Setting_Value_Bottom_Limit>0)
                  Setting_Value_Temp=Setting_Value_Top_Limit;
            else
                  Setting_Value_Temp=Setting_Value_Bottom_Limit;
        }
				Setting.Setting_ReLoad_Mask=1;
				Work_Status.SYS_Setting_OverFlow=0;
			}
		break;
		case KEY_VALUE_DOWN_S://短按 down
			if(Setting.In_Flag==0)
			{
//					if(PAS_Value>(Work_Status.SCA_Leve_Setting%2))
					if(PAS_Value>0)
					{
						PAS_Value--;
						if(PAS_Scroll_Step==0)
							PAS_Scroll_Step = 5;
						else
							PAS_Scroll_Step = 6;
//						LCD_DisPlay.PAS_ReLoad_Flag=1;
					}
					else
					{
						PAS_Scroll_Step = 5;
//						PAS_Value_NG=1;
					}
			}
			else
			{
				if(Setting_Value_Temp>Setting_Value_Bottom_Limit)
            Setting_Value_Temp--;
        else
        {
            if(Setting_Value_Bottom_Limit>0)
                  Setting_Value_Temp=Setting_Value_Bottom_Limit;
            else
                  Setting_Value_Temp=Setting_Value_Top_Limit;
        }
				Setting.Setting_ReLoad_Mask=1;
				Work_Status.SYS_Setting_OverFlow=0;
			}
		break;
		case KEY_VALUE_i_L://长按 i
			if(Setting.In_Flag!=0)
			{
				Setting.Setting_ReLoad_Mask = 1;
				Setting.Exit_Flag=1;
			}
		break;
		case KEY_VALUE_DOWN_L://长按 DOWN  不保存设置，退出设置界面
			if(Setting.In_Flag!=0 && Code_Input_Flag == 0)
			{
				Setting.Mode=0;
				Setting.In_Flag=0;
				Setting.Exit_Flag=1;
				if(Work_Status.SYS_Err_Occurred_Flag)
					LCD_DisPlay.ERR_ReLoad_Flag=1;
			}
		break;
		#if KEY_SCAN_EN
		case KEY_VALUE_LT_L://大灯 开 关  
		case KEY_VALUE_LT_S://大灯 开 关  
		#else
		case KEY_VALUE_UP_L://大灯 开 关  
		#endif
			if(Work_Status.Light_Switch_Flag)
			{
				Work_Status.Light_Switch_Flag=0;
			}
			else
			{
				Work_Status.Light_Switch_Flag=1;
			}
			Work_Status.Light_Sensing_EN = 0;
			LCD_DisPlay.Light_ReLoad_Flag = 1;
			if(Work_Status.Light_Switch_Flag)
				Set_BK_Light_Leve(1);
			else
				Set_BK_Light_Leve(Work_Status.BLK_Light_Level);
//				Set_BK_Light_Leve(5);
		break;
//		case KEY_VALUE_PW_L://关机
//			if((!Work_Status.SYS_Power_Just_ON) && (EMC_TEST.EN_Flag == 0))
//			{
//				Work_Status.ShutDown_Flag = 1;
//			}
//		break;
		default:break;
	}
}
/*----System ShutDown-----**关机操作**--------*/
void Sys_ShutUP_I(void)
{
	//LCD_Clear(Black);
	//delay_ms(1000);
	Set_BK_Light_Leve(0);//关闭背光
	LCD_Clear(Black);
	Work_Status.SYS_Power_States_Flag=0;
	HAL_TIM_Base_Stop(&htim2);//关闭定时器
	HAL_TIM_Base_Stop(&htim3);
	Read_Save_Total_Trap_Record(0);//保存数据
	Read_Save_Single_Trap(0);
	Read_Save_MAX_Trap(0);
	delay_ms(5);
	Save_Total_Trap_Bak();
	delay_ms(5);
	PAS_Value=0;//发送0档
	System_Power_OFF;
	while(!Key_MOD)//等待按键松开
	{
		if(MY_USART.USART_Send_EN)
		{
				MY_USART.USART_Send_EN = 0;
				DisPlay_Send_Running_Data();
		}
	}
	while(1)
	{
		if(!Key_MOD)//等待按键松开
		{
			delay_ms(100);
		}
	}
}
/*----System Sleep -----**-USB充电模式下的系统休眠-**-------*/
void Sys_Sleep_USB(void)
{
	u8 s = 0 , PAS_USB_Buffer;
	PAS_USB_Buffer = PAS_Value;
	LCD_Clear(Black);
	DiaPlay_USB(1);
	Set_BK_Light_Leve(1);
	Show_Word_Agency(10,240,0,(u8 *)"USB Charging..." , White , Black);
	PAS_Value = 0;
	while(1)
	{
		if(!Work_Status.USB_State_Flag)
			Sys_ShutUP_I();
		if(MY_USART.USART_Send_EN)
		{
				MY_USART.USART_Send_EN = 0;
				DisPlay_Send_Running_Data();
		}
		if(!Key_MOD)
		{
			if(++s > 100)
			{
				s = 0;
				PAS_Value = PAS_USB_Buffer;
				Work_Status.ShutDown_Flag = 0;
				Work_Status.USB_Sleep_Flag = 0;
				Sys_Auto_ShutUp_Delay = 0;
				LCD_DisPlay.USB_ReLoad_Flag = 1;
				Work_Status.SYS_Err_Occurred_Flag=0;
				Set_BK_Light_Leve(Work_Status.BLK_Light_Level);
				Int_Index_DisPlay();
				break;
			}
		}
		delay_ms(10);
	}
}
/*******
*定时中断处理子函数*-->[ 10mS ]<--
*******/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	uint8_t  i,Key_Temp;
    if(htim->Instance == htim2.Instance)
    {
		if(Work_Status.Bike_Moving_Flag)//如果车子在动，则计算里程
		{
			if(++Mileage_Variable.Base_Time_Count_NUM > Mileage_Variable.Base_Time_Count_Delay)//基本时基单元（车轮转动一圈的时间）
			{
				Mileage_Variable.Base_Time_Count_NUM=0;
				Mileage_Variable.Base_mm_Count += Wheel_Perimeter;//时间到，里程增加一个车轮周长
				if(Mileage_Variable.Base_mm_Count>10000)//100m = 0.1Km  <1mS: 100000 = 100m>  <10mS: 10000 = 100m>
				{
					Mileage_Variable.Base_mm_Count-=10000;
					if(Sport_Recording.Total_Trap_Record<10000000)//最大可计数到 1000000.0Km
						Sport_Recording.Total_Trap_Record++;
					if(++Sport_Recording.Service_Milage_ADD>10)
					{
						Sport_Recording.Service_Milage_ADD = 0;
					}
					if(Sport_Recording.Single_Trap_At_Once<60000)//单次里程，每6000Km自动清零
						Sport_Recording.Single_Trap_At_Once++;
					else
					{
						Sport_Recording.Single_Trap_At_Once=0;
						Sport_Recording.Timer_Of_This_Trap=0;
					}
					if((Work_Status.Working_Mode == 0) || (Work_Status.Working_Mode == 1))
						Work_Status.Interface_ReLoad_Mask=1;//刷新显示
//					LCD_DisPlay.TRIP_Dis_Flag = 1;
					if(++Trip_Record_Save_Delay>50)//每5km保存一次里程数据
					{
						Trip_Record_Save_Delay = 0;
						Work_Status.Save_Datas_Flag = 1;
					}
					Avg_JS_Flag = 1;
					Work_Status.Moving_State_Chg = 1;
				}
			}
			Sys_Auto_ShutUp_Delay = 0;
		}
		else
		{
			if(Work_Status.Moving_State_Chg)//车次由运动状态转变为静止后，保存一次数据
			{
				Work_Status.Moving_State_Chg = 0;
				Work_Status.Save_Datas_Flag = 1;
			}
			if((Motor_Power_CC < 10) && (EMC_TEST.EN_Flag == 0))
			{
				if(SYS_Auto_OFF > 0)
				{
					if(++Sys_Auto_ShutUp_Delay>5900 * SYS_Auto_OFF)//自动关机 计时   SYS_Auto_OFF
					{
						Work_Status.ShutDown_Flag = 1;
						if(Work_Status.USB_State_Flag)
							Work_Status.USB_Sleep_Flag = 1;
					}
				}
			}
			else
				Sys_Auto_ShutUp_Delay = 0;
			if(++AVG_Dec_Delay>150)
			{
				Avg_JS_Flag = 1;
				AVG_Dec_Delay = 0;
			}
		}
		
		if(++MY_USART.Send_Time_Delay>45)
		{
			MY_USART.Send_Time_Delay = 0;
			MY_USART.USART_Send_EN = 1;
		}
		
		if((!Work_Status.USB_Sleep_Flag)&& Work_Status.SYS_Power_States_Flag)
			Key_Temp = Key_Scan_Handler();
		else
			Key_Temp = 0;
		if(Work_Status.P_Walk_Flag)//Walk 模式下，按键值返回 0 则退出Walk模式
		{
			#if KEY_SCAN_EN
			if((Key_Temp&0x02)==0)
			#else
			if((Key_Temp&0x08)==0)
			#endif
			{
				if(++WALK_EXIT_DELAY>5)
				{
					WALK_EXIT_DELAY = 0;
					LCD_DisPlay.WALK_ReLoad_Flag = 1;
					Work_Status.P_Walk_Flag=0;
				}
			}
			else
				WALK_EXIT_DELAY = 0;
		}
		if(Key_Temp&0x80)//存在有效按键
		{
			Sys_Auto_ShutUp_Delay=0;//清除自动关机 计时
			#if KEY_SCAN_EN
			if(Key_Temp==0xc2  && (Setting.In_Flag==0))
			#else
			if(Key_Temp==KEY_VALUE_WK_L  && (Setting.In_Flag==0))
			#endif
			{
				LCD_DisPlay.WALK_ReLoad_Flag = 1;
				Work_Status.P_Walk_Flag=1;
			}
			Key_Mission_Process(Key_Temp);
		}
		if(Work_Status.SYS_Power_Just_ON)
		{
			if(Key_Temp==0 && Work_Status.SYS_Power_States_Flag)
				Work_Status.SYS_Power_Just_ON=0;
		}
		else
		{
			if(!Key_MOD)
			{
					if(++Shut_SYS_Delay>180)////////////////////////////////////////////////////原来是180，调试改为80
						Work_Status.ShutDown_Flag = 1;
			}
			else
				Shut_SYS_Delay = 0;
		}
		/*串口接收结束判断----------------------------------------------------***  -USART Recive END -  ***--------                                                      */
		Rx_Cnt = USART_Get_NUMS();
		if(Rx_Cnt>0)
		{
			if(Rx_Cnt != Rx_Cnt_Last)
			{
					Rx_Cnt_Last = Rx_Cnt;
					Rx_Time_Over = 0;
			}
			else
			{
					if(++Rx_Time_Over>USART1_RXTime_OUT)
					{
						Rx_Time_Over = 0;
						HAL_UART_DMAStop(&huart1);
						Rx_Len = USART_Get_NUMS();
						RX_NUM = Rx_Len;
						for(i=0;i<Rx_Len;i++)
							Rx_Buf[i]=Rx_DMA_Buf[i];
						RX_Flag = 1;
						HAL_UART_Receive_DMA(&huart1, (uint8_t *)Rx_DMA_Buf , 128);
					}
			}
			
		}
		else
		{
			Rx_Time_Over = 0;
			Rx_Cnt_Last = 0;
		}
		/*走时函数-------------------------------------------------------------*****   - Timer -  *****     ---------                                                       */
		if(++Timer_Second_Delay>97)
		{
			Timer_Second_Delay = 0;
			if(Timer_Second < 59)
				Timer_Second++;
			else
			{
				Timer_Second = 0;
				if(Timer_Minute<59)
				{
					Timer_Minute++;
					if(Time_For_BF_ShutWithClear<9000)
						Time_For_BF_ShutWithClear++;
					if(++Sport_Recording.Timer_Of_This_Trap > 50000)
					{
						Sport_Recording.Timer_Of_This_Trap = 0;
						Sport_Recording.Single_Trap_At_Once = 0;
					}
					if(Work_Status.Working_Mode==4)
						Work_Status.Interface_ReLoad_Mask = 1;
				}
				else
				{
					Timer_Minute = 0;
					if(Timer_Hour<23)
						Timer_Hour++;
					else
						Timer_Hour=0;
				}
				LCD_DisPlay.TIME_ReLoad_Flag = 1;
			}
		}
		if(++D500mS_Count>50)
		{
			D500mS_Count = 0;
			Work_Status.See_Volatge_Flag = 1;
			if(Setting.In_Flag && !Setting.Start_Code_Flag)//设置模式下，超过1min无操作，自动退出
			{
				if(++Work_Status.SYS_Setting_OverFlow>40)
				{
					Work_Status.SYS_Setting_OverFlow=0;
					Setting.Mode=0;
					Setting.Chosen = 0;
					Setting.In_Flag=0;
					Setting.Exit_Flag=1;
					Work_Status.Call_IN_From_PC_Flag=0;
					if(Work_Status.SYS_Err_Occurred_Flag)
						LCD_DisPlay.ERR_ReLoad_Flag=1;
				}
			}
			if((Work_Status.Current_Err_CODE != 0x30)&&(Setting.In_Flag==0)&&(Work_Status.SYS_Start_ON_Flag))
			{
				if((++Work_Status.NoCommunication_OverFlow>NoCommunication_OverStand))//通讯超时   检测
				{
					LCD_DisPlay.ERR_ReLoad_Flag = 1;
					Work_Status.NoCommunication_OverFlow=0;
					Work_Status.Current_Err_CODE=0x30;
					Battery_SOC = 0;
					Battery_SOC_FR = Battery_SOC;
					Work_Status.Current_Speed=0;//通讯失败，速度、功率清零
					Motor_Power_CC_Br=0;
					Motor_Power_CC=0;
					LCD_DisPlay.Bat_ReLoad_Flag = 1;
					LCD_DisPlay.Spd_ReLoad_Flag=1;
					LCD_DisPlay.PCC_ReLoad_Flag=1;
					Work_Status.Bike_Moving_Flag=0;
					Mileage_Variable.Base_Time_Count_Delay=60000;
					D_Speed=0;
					Speed_Buf=0;
				}
			}
			if(Setting.PW_Set_MSG_Flag>0)
			{
					if(++Setting.PW_Set_MSG_Delay>3)
					{
							Setting.PW_Set_MSG_Flag = 0;
							Setting.PW_Set_MSG_Delay = 0;
							Setting.UpDat_Flag = 1;
							Setting.Setting_ReLoad_Mask=1;
							Work_Status.SYS_Setting_OverFlow=0;
					}
			}
		}
//		if(++Speed_ReLoad_Delay>1)//速度渐变刷新  SPD_buf 
//		{
//			Speed_ReLoad_Delay=0;
//			if((Speed_Buf>0) || (D_Speed>0))
//				LCD_DisPlay.Spd_ReLoad_Flag=1;
//		}
		if(USB_Ts)
		{
			if(Work_Status.USB_State_Flag)
			{
				if(++USB_Check_Delay>50)
				{
					USB_Check_Delay = 0;
					Work_Status.USB_State_Flag = 0;
					LCD_DisPlay.USB_ReLoad_Flag = 1;
				}
			}
			USB_Relase_Delay = 0;
		}
		else
		{
			if(!Work_Status.USB_State_Flag)
			{
				if(++USB_Relase_Delay>50)
				{
					USB_Relase_Delay = 0;
					Work_Status.USB_State_Flag = 1;
					LCD_DisPlay.USB_ReLoad_Flag = 1;
				}
			}
			USB_Check_Delay = 0;
		}
		
		if(SYS_Voltage_Start > SYS_Voltage_Fr)
		{
			if(++Voltage_DEC_Delay>Voltage_DEC_Base)
			{
				Voltage_DEC_Delay=0;
				SYS_Voltage_Start--;
				LCD_DisPlay.Bat_ReLoad_Flag=1;
			}
		}
		if(PAS_Scroll_Step>0)
		{
			if(++PAS_Scroll_Delay>2)
			{
				PAS_Scroll_Delay = 0;
				PAS_Scroll_Step--;
				LCD_DisPlay.PAS_ReLoad_Flag=1;
			}
		}
	}
}
/*开机密码输入界面*/
void DisPlay_PassWord_InView(void)
{
	unsigned char P_s = 0 , i , GJ_Delays = 0 , GJ_EN = 0;
	u16 CODE_IN_TIME_OVER=0;
	PAS_Value = 0;	
	PW_Send_Flag = 1;
	Work_Status.USB_Sleep_Flag = 0;
	Work_Status.SYS_Power_States_Flag = 1;
	Setting.Start_Code_Flag = 1;
	User_Input_Code_Tab[0]=10;
	User_Input_Code_Tab[1]=10;
	User_Input_Code_Tab[2]=10;
	User_Input_Code_Tab[3]=10;
	Setting.In_Flag = 1;
	Setting.Mode = 1;
	Setting.Selected = 4;
	Code_Input_Flag = 1;
	Setting.Setting_ReLoad_Mask = 1;
	Setting.Card_ReLoad_Flag = 1;
	LCD_Init();
//	#ifdef EV_BIKE_LOGO
		LCD_Clear(Black);
//	#else
//		LCD_Clear(Green);
//	#endif
	Dis_Play_PassWord_Ground(1);
	DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , 0);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	Set_BK_Light_Leve(Work_Status.BLK_Light_Level);//打开背光
	while(1)//等待正确密码
	{
		if(Code_Input_Flag<5)
		{
			if(Setting.Setting_ReLoad_Mask)
			{
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					Setting_Value_Bottom_Limit=0;
					Setting_Value_Temp=User_Input_Code_Tab[Code_Input_Flag-1];
					Setting_Value_Top_Limit=9;
				}
				User_Input_Code_Tab[Code_Input_Flag-1]=Setting_Value_Temp;
				DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , 0);
				Setting.Setting_ReLoad_Mask=0;
				CODE_IN_TIME_OVER = 0;
			}
		}
		else
		{
			if((User_Input_Code_Tab[0]==Sys_Code_Tab[0])&&(User_Input_Code_Tab[1]==Sys_Code_Tab[1])\
				&&(User_Input_Code_Tab[2]==Sys_Code_Tab[2])&&(User_Input_Code_Tab[3]==Sys_Code_Tab[3]))
			{
				Setting.Mode = 0;
				Setting.In_Flag = 0;
				Code_Input_Flag = 0;
				Setting.Selected = 0;
				PAS_Value = DEFAULT_START_PAS;
				Setting.Start_Code_Flag = 0;
				PW_Send_Flag = 0;
				Sys_Auto_ShutUp_Delay = 0;
				Work_Status.ShutDown_Flag = 0;//清零自动关机
				break;
			}
			else
			{
				Code_Input_Flag = 1;
				DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , 0);
				if(++P_s>2)
				{
					Show_Word_Agency(10,320,0,"Bye-bye~              ",Black,White);
					Show_Word_Agency(10,285,0,"                      ",Black,White);
					delay_ms(800);
					Set_BK_Light_Leve(0);
					LCD_Clear(Black);
					Work_Status.SYS_Power_States_Flag=0;
					HAL_TIM_Base_Stop(&htim2);//关闭定时器
					HAL_TIM_Base_Stop(&htim3);
					delay_ms(5);
					System_Power_OFF;
				}
				else
				{
					Show_Word_Agency(10,320,0,"Password Incorrect!",Black,White);
					if(P_s == 1)
						Show_Word_Agency(10,285,0,"Remaining -2- times",NBlack,White);
					else if(P_s == 2)
						Show_Word_Agency(10,285,0,"Remaining -1- time  ",NBlack,White);
					for(i=0;i<4;i++)
						User_Input_Code_Tab[i]=10;
				}
			}
		}
		if(!Key_MOD)
		{
			if(GJ_EN)
			{
				if(++GJ_Delays>180)
				{
					Sys_ShutUP_I();
				}
			}
		}
		else
		{
			GJ_Delays = 0;
			GJ_EN = 1;
		}
		if(++CODE_IN_TIME_OVER>6000)
			System_Power_OFF;
		delay_ms(10);
	}
	Work_Status.SYS_Power_States_Flag = 0;
}
/* USER CODE END 0 */
int main(void)
{
  /* USER CODE BEGIN 1 */
	
	uint16_t i;
	uint32_t P_s;
	SCB->VTOR = FLASH_BASE | 0x10000;
	Start_MCU();
	LCD_Init();
	LCD_Clear(Def_BGC);
	SaveAndRead_Data_By_aBigTab(0x87);//读取数据
	S_Num_Err = Error_Code_Tab[10];
	delay_ms(5);
	Initialization_System_Start();//系统参数初始化
	
//	SYS_Pus_CPn = 6;//速度传感器磁钢数
	SYS_HAND_F_Flag = 0;//转把分档
	SYS_HAND_L_Flag = 0;//转把助力推行
	
	#if KEY_SCAN_EN==0
	Set_Walk_KEY_IN();
	#endif
	delay_ms(2);
	System_Power_ON;//系统上电
	if(Work_Status.SYS_Start_PassWord_EnAble)/*开机密码*/
		DisPlay_PassWord_InView();
	PW_Send_Flag = 0;
	
	delay_ms(30);
	MX_TIM3_Init();
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	Set_BK_Light_Leve(Work_Status.BLK_Light_Level);//打开背光
	Work_Status.SYS_Start_ON_Flag = 1;
	Work_Status.SYS_Power_States_Flag = 1;
	
	P_s = 30;
	PAS_V_Last = 0;
	PAS_Scroll_Step = 4;
	#ifdef Start_LOGO_EN
	Dis_LOGO();
	#endif
	while(ADC_Value[147] == 0);
	P_s = 0;
	for(i=0;i<50;i++)
		P_s += ADC_Value[3*i];
	SYS_Voltage_Start = P_s/V_Standard_Value;
	SYS_Voltage_Fr = SYS_Voltage_Start;
	MY_USART.Send_States_Flag=0;
	EMC_TEST.Start_Delay = 18000;
	EMC_TEST.EN_Flag = 0;
	EMC_TEST.Check_Delay = 0;
	#ifdef Start_LOGO_EN
	delay_ms(500);
	#endif
	Int_Index_DisPlay();/*页面显示初始化*/
	Work_Status.SYS_Start_ON_Flag = 1;
	Work_Status.SYS_Power_States_Flag = 1;
	Battery_SOC_Start = 1;
	
  while (1)
  {
		if(RX_Flag)
		{
			RX_Flag=0;
			J_DisPlay_Receive_CallBack();
		}
		if(MY_USART.USART_Send_EN)
		{
				MY_USART.USART_Send_EN = 0;
				DisPlay_Send_Running_Data();
		}
		if(Setting.In_Flag==0)
		{
			LCD_DisPlay_ReLoad();
		}
		else
		{
			Setting_Menu_List();
		}
		
		if(Work_Status.See_Volatge_Flag)
		{
			Work_Status.See_Volatge_Flag = 0;
			P_s = 0;
			for(i=0;i<50;i++)
				P_s += ADC_Value[3*i];
			SYS_Voltage = (P_s/V_Standard_Value) + 1;
			
			if(C_Current>20)//电流大于7A
				SYS_Voltage += (C_Current*2/3);
			
			if(SYS_Voltage_Fr > SYS_Voltage)	
			{
				if(++Voltage_Check_SS>DL_Delay_Time)//????
				{
					Voltage_Check_SS=0;
					SYS_Voltage_Fr=SYS_Voltage;
					SYS_Voltage_Dec=SYS_Voltage_Fr - SYS_Voltage;
					if(SYS_Voltage_Dec>24)
						SYS_Voltage_Dec=50;
					else
						SYS_Voltage_Dec*=2;
					Voltage_DEC_Base=(75-SYS_Voltage_Dec);
				}
			}
			else
			{
				Voltage_Check_SS=0;
				if(SYS_Voltage>SYS_Voltage_Start)
					Voltage_DEC_Delay=0;
			}
			if(Work_Status.Light_Sensing_EN)//光感使能与否
			{
				P_s = 0;
				for(i=0;i<50;i++)
					P_s += ADC_Value[2 + (3*i)];
				P_s/=50;
				if(Work_Status.Light_Sensing_Flag)
				{
					if(P_s < LIGHT_OFF_SET)
					{
						if(++Work_Status.Light_Sense_Filter>2)
						{
							Work_Status.Light_Sensing_Flag = 0;
							Work_Status.Light_Switch_Flag=0;
							LCD_DisPlay.Light_ReLoad_Flag = 1;
							Set_BK_Light_Leve(Work_Status.BLK_Light_Level);
						}
					}
					else
						Work_Status.Light_Sense_Filter = 0;
				}
				else
				{
					if(P_s > LIGHT_ON_SET)
					{
						if(++Work_Status.Light_Sense_Filter>2)
						{
							Work_Status.Light_Sensing_Flag = 1;
							Work_Status.Light_Switch_Flag=1;
							LCD_DisPlay.Light_ReLoad_Flag = 1;
							Set_BK_Light_Leve(1);
						}
					}
					else
						Work_Status.Light_Sense_Filter = 0;
				}
			}
		}
		
		if(Work_Status.ShutDown_Flag)
		{
			Work_Status.ShutDown_Flag = 0;
			if(Work_Status.USB_Sleep_Flag)//充电状态下休眠
				Sys_Sleep_USB();
			else
				Sys_ShutUP_I();
		}
		
		if(Work_Status.Save_Datas_Flag)
		{
			Work_Status.Save_Datas_Flag = 0;
			Read_Save_Total_Trap_Record(0);//保存数据
			Read_Save_Single_Trap(0);
			if(Work_Status.MAX_SPD_Reset)
			{
				Work_Status.MAX_SPD_Reset=0;
				Read_Save_MAX_Trap(0);
			}
		}
  }
//	#endif
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
//  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 240;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
/**
  * @brief  Early Wakeup WWDG callback.
  * @param  hwwdg: pointer to a WWDG_HandleTypeDef structure that contains
  *              the configuration information for the specified WWDG module.
  * @retval None
  */
void HAL_WWDG_WakeupCallback(WWDG_HandleTypeDef* hwwdg)
{
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_WWDG_WakeupCallback could be implemented in the user file
   */
    /*##-2- Refresh the WWDG #####################################################*/
    HAL_WWDG_Refresh(hwwdg, 127);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
