#ifndef __MYIIC_H
#define __MYIIC_H
#include "sys.h" 

//IO方向设置
#define SDA_IN()  {GPIOC->MODER&=~(3);GPIOC->MODER|=0;}	//PA11输入模式
#define SDA_OUT() {GPIOC->MODER&=~(3);GPIOC->MODER|=1;} //PA11输出模式
//IO操作函数	 
#define IIC_SCL_1      HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_SET)
#define IIC_SCL_0      HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_RESET)

#define IIC_SDA_1      HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET)
#define IIC_SDA_0      HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET)

#define READ_SDA		HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)
//#define IIC_SDA    PCout(0) //SDA	 
//#define READ_SDA   PCin(0)  //输入SDA 

//IIC所有操作函数
//void IIC_Init(void);                //初始化IIC的IO口				 
void IIC_Start(void);				//发送IIC开始信号
void IIC_Stop(void);	  			//发送IIC停止信号
void IIC_Send_Byte(u8 txd);			//IIC发送一个字节
uint8_t IIC_Read_Byte(unsigned char ack);//IIC读取一个字节
uint8_t IIC_Wait_Ack(void); 				//IIC等待ACK信号
void IIC_Ack(void);					//IIC发送ACK信号
void IIC_NAck(void);				//IIC不发送ACK信号

void IIC_Write_One_Byte(uint8_t daddr,uint8_t addr,uint8_t data);
uint8_t IIC_Read_One_Byte(uint8_t daddr,uint8_t addr);

#endif
















