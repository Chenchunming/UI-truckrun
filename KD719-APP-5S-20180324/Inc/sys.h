#ifndef __SYS_H
#define __SYS_H	 
//#include "stm32f4xx.h"
#include "stm32f2xx.h"
#include "24cxx.h"
//#include "led.h"

#define KEY_SCAN_EN 1

/*部分功能设定*/
//#define LIGHT_SHORT_PRESS_ON  //短按可开启夜间模式  否则：长按 可 开启 亦可关闭  ；短按只能关闭

/*按键键值定义*/
#if KEY_SCAN_EN
#define KEY_VALUE_UP_S      0x84  //up键 短按
#define KEY_VALUE_DOWN_S    0x82  //down键 短按
#define KEY_VALUE_i_S       0xA0  //i键 短按
#define KEY_VALUE_LT_S      0x90  //Light键 短按
#define KEY_VALUE_UP_L      0xC4  //up键 长按
#define KEY_VALUE_DOWN_L    0xC2  //down键 长按
#define KEY_VALUE_i_L       0xE0  //i键 长按
#define KEY_VALUE_LT_L      0xD0  //Light键 长按
#define KEY_VALUE_PW_L      0xC1  //关机键 长按
#else
#define KEY_VALUE_UP_S      0x84  //up键 短按
#define KEY_VALUE_DOWN_S    0x82  //down键 短按
#define KEY_VALUE_i_S       0x81  //i键 短按
//#define KEY_VALUE_LT_S      0x90  //Light键 短按
#define KEY_VALUE_UP_L      0xC4  //up键 长按
#define KEY_VALUE_DOWN_L    0xC2  //down键 长按
#define KEY_VALUE_i_L       0xc1  //i键 长按
#define KEY_VALUE_WK_L      0xC8  //Light键 长按
//#define KEY_VALUE_PW_L      0xC1  //关机键 长按
#endif

//#define KP_ON  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET)
//#define KP_OFF HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET)


typedef  void (*iapfun)(void);				//定义一个函数类型的参数.   
#define  BOOT_LOAD_ADDR		0x08000000
#define  MY_SETTING_INDEX	100
#define  Advanced_Pas_Ent	65
	 
//位带操作,实现51类似的GPIO控制功能
//具体实现思想,参考<<CM3权威指南>>第五章(87页~92页).M4同M3类似,只是寄存器地址变了.
//IO口操作宏定义
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 
//IO口地址映射
#define GPIOA_ODR_Addr    (GPIOA_BASE+20) //0x40020014
#define GPIOB_ODR_Addr    (GPIOB_BASE+20) //0x40020414 
#define GPIOC_ODR_Addr    (GPIOC_BASE+20) //0x40020814 
#define GPIOD_ODR_Addr    (GPIOD_BASE+20) //0x40020C14 
#define GPIOE_ODR_Addr    (GPIOE_BASE+20) //0x40021014 
#define GPIOF_ODR_Addr    (GPIOF_BASE+20) //0x40021414    
#define GPIOG_ODR_Addr    (GPIOG_BASE+20) //0x40021814   
#define GPIOH_ODR_Addr    (GPIOH_BASE+20) //0x40021C14    
#define GPIOI_ODR_Addr    (GPIOI_BASE+20) //0x40022014     

#define GPIOA_IDR_Addr    (GPIOA_BASE+16) //0x40020010 
#define GPIOB_IDR_Addr    (GPIOB_BASE+16) //0x40020410 
#define GPIOC_IDR_Addr    (GPIOC_BASE+16) //0x40020810 
#define GPIOD_IDR_Addr    (GPIOD_BASE+16) //0x40020C10 
#define GPIOE_IDR_Addr    (GPIOE_BASE+16) //0x40021010 
#define GPIOF_IDR_Addr    (GPIOF_BASE+16) //0x40021410 
#define GPIOG_IDR_Addr    (GPIOG_BASE+16) //0x40021810 
#define GPIOH_IDR_Addr    (GPIOH_BASE+16) //0x40021C10 
#define GPIOI_IDR_Addr    (GPIOI_BASE+16) //0x40022010 
 
//IO口操作,只对单一的IO口!
//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

#define PHout(n)   BIT_ADDR(GPIOH_ODR_Addr,n)  //输出 
#define PHin(n)    BIT_ADDR(GPIOH_IDR_Addr,n)  //输入

#define PIout(n)   BIT_ADDR(GPIOI_ODR_Addr,n)  //输出 
#define PIin(n)    BIT_ADDR(GPIOI_IDR_Addr,n)  //输入

#define Key_MOD  HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2)
#define Key_L2   HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3)
#define Key_L3   HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4)
#define Key_R1   HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_5)

#define Key_R_1  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET);
#define Key_R_0  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET);

#define USB_Ts   HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_10)
//#define Sys_KP  PAout(8)
//#define BG_PWM  PAout(7)

void WFI_SET(void);		//执行WFI指令
void INTX_DISABLE(void);//关闭所有中断
void INTX_ENABLE(void);	//开启所有中断
void MSR_MSP(u32 addr);	//设置堆栈地址 
void iap_load_app(u32 appxaddr);
//void Sys_Soft_Reset(void);

u8   Key_Scan_Handler(void);//按键扫描程序
u8 SPEED_UNIT_CONVERT(u8 State , u8 Dat_IN);//速度公英制转换
//void Int_Sys_Values(void);
#endif






